/*

                                    ##                                        
                             ##    ###   ###                                  
                            ##           ##   #                               
                   ####                     ###                               
                                                                              
             ##                                                               
           ####                                                               
                       #######   ###    ###       ###      ########    #######
                      ####  #    ###    ###      #####     #### ####   ####   
     ###              ####       ###### ###     ### ###    ###   ###   ###    
    ###                ######    ##########     ##  ###    ########    #######
                          ####   ###    ###    #########   #### ###    ###    
   #                   ##  ###   ###    ###   ###########  ###   ###   #######
####                  #######    ###    ###   ###     ###  ###   ###   #######
#                                                                             
                             
 ###                    #    
###                   #####             - WAVE 9  -
     ###      ###     ###                   
   ####     ####                        -   All Countries   -
   #        ##               
                             

*Author: Tessa-Virginia Hannemann, Michael Bergmann
*Last updated: Oct 2021
*Source: Release 0 w91


*----[ O v e r v i e w  o f  C o n t e n t s ]---------------------------------
**** 1) Data Cleaning
**** 2) Entire Sample (by country)
**** 3) Male vs. Female (by country)
**** 4) By age groups (by country)
*/


*----[Stata Version & Settings]-------------------------------------------------
version 14
clear               
clear matrix
clear mata        
set more off        
set maxvar 32000
set scrollbufsize 2000000   
set scheme s1mono


*----------------[Globals]------------------------------------------------------
** "<SHARE Data>" needs to be replaced with the path to the acutal SHARE data 
** "<Project path>" needs to be replaced with the actual project path
global c_all  "at be_fr be_nl bg ch cy cz de dk ee   es fi fr gr hr hu il it lt lu lv mt nl pl pt ro se si sk"
global w1 "<SHARE Data>\Release_7-1-0\stata\sharew1_rel7-1-0_ALL_datasets_stata"
global w2 "<SHARE Data>\Release_7-1-0\stata\sharew2_rel7-1-0_ALL_datasets_stata"
global w3 "<SHARE Data>\Release_7-1-0\stata\sharew3_rel7-1-0_ALL_datasets_stata"
global w4 "<SHARE Data>\Release_7-1-0\stata\sharew4_rel7-1-0_ALL_datasets_stata"
global w5 "<SHARE Data>\Release_7-1-0\stata\sharew5_rel7-1-0_ALL_datasets_stata"
global w6 "<SHARE Data>\Release_7-1-0\stata\sharew6_rel7-1-0_ALL_datasets_stata"
global w7 "<SHARE Data>\Release_7-1-0\stata\sharew7_rel7-1-0_ALL_datasets_stata"
global wX "<SHARE Data>\Release_7-1-0\stata\sharewX_rel7-1-0_gv_allwaves_cv_r_stata"
global w8 "<SHARE Data>\Release_8-0-0\stata\sharew8_rel1-0-0_ALL_datasets_stata"
global scs1 "<SHARE Data>\Release_8-0-0\stata\sharew8_rel1-0-0_ca_ALL_datasets_stata"
global scs2 "<SHARE Data>\sharew91_rel0_ALL_datasets_stata"
global data "<Project path>\_Data\Vaccination\Rel0"
global temp "<Project path>h\_Data\Vaccination\Rel0\temp"
global world "<Project path>\_maps\world"
global map "<Project path>\_maps"
global output "<Project path>\graphs\Rel0"
global path "<Project path>"



********************************************************************************
************************* 1) DATA CLEANING *************************************
********************************************************************************

use "${scs2}/sharew91_rel0_ca.dta", clear
merge 1:1 mergeid using "${scs2}/sharew91_rel0_cv_r.dta", nogen keep(1 3)
	drop rel_*
merge 1:1 mergeid using "${scs2}/sharew91_rel0_gv_weights.dta", nogen keep(1 3)


drop if cciw_w91==.
count	// 47,176 obs.

* Age
tab cadn003_, m
gen agecat = .
replace agecat = 1 if inrange(cadn003_, 1957, 1971)
replace agecat = 2 if inrange(cadn003_, 1942, 1956)
replace agecat = 3 if cadn003_ <=1941
replace agecat = . if cadn003_ == -1 | cadn003_ == -2
lab def agecat 1 "50-64" 2 "65-79" 3 "80 and above"
lab val agecat agecat
numlabel agecat, add
tab cadn003_ agecat, m
tab agecat, gen(agecat)

* Country
tab country, m

* Gender
tab cadn042_, m
recode cadn042_ (1=0)(2=1), gen(female)
lab var female "Sex: female"
tab female, m
tab female, gen(female)

* Vaccination
tab cahc117_, m		 
recode cahc117_ (-1 -2=.) (5=0), gen(vax)
tab cahc117_ vax, row m

tab cahc118_, m
recode cahc118_ (-9=.c)(-2=.b)(1 2=2)(3=0)(4 -1=1), gen(vax_int)	// dk set to 1!
lab def vax_int 0 "no" 1 "still undecided" 2 "yes"
lab val vax_int vax_int
numlabel vax_int, add
tab cahc118_ vax_int, row m
tab vax_int, m

tab vax_int vax, m

gen vax2 = 1 if vax==1
replace vax2 = 2 if vax==0 & vax_int==2
replace vax2 = 3 if vax==0 & vax_int==1
replace vax2 = 4 if vax==0 & vax_int==0
lab var vax2 "Vaccination & intention"
lab def vax2 1 "I am already vaccinated" ///
	2 "I want to be vaccinated/have already scheduled a vaccination" ///
	3 "I'm still undecided" 4 "I don't want to be vaccinated"
lab val vax2 vax2
numlabel vax2, add
tab vax2, m

tab vax2, gen(vax_int)
tab1 vax_int?, m

	
save "${data}\Vaccinations.dta", replace





********************************************************************************
*************************	MAPS    ********************************************
********************************************************************************

do ${path}\programs\1_maps_database_v2.do
do ${path}\programs\2_maps_graphs.do

