

******* GRAPHS OVERALL ***********


spmap no_vax using "${map}/SHARE_map.dta",id (_ID) clmethod(custom) clnumber(5) clbreaks(0 .2 .4 .6 .8) ///
	 fcolor (OrRd) ndfcolor(gs13) legend(label(1 "non SHARE country") label(2 "0-20%") label(3 "20-40%" ) label(4 "40-60%") label(5 "60% and more")) ///
	legend(pos(10)) /// 
		legend(size(1.7))

graph save "${output}/novax.gph", replace

/*
spmap no_vax_a50 using "${map}/SHARE_map.dta",id (_ID) clmethod(custom) clnumber(8) clbreaks(0 .1 .2 .3 .4 .5 .6 .7 .72) ///
	 fcolor (OrRd) ndfcolor(white) legend(label(1 "no data") label(2 "0-10%") label(3 "10-20%" ) label(4 "20-30%") label(5 "30-40%") lab(6 "40-50") lab(7 "50-60%") lab(8 "60-70%") lab(9 "over 70%")) ///
	legend(pos(10)) /// 
		legend(size(1.7)) title ("Respondents, aged 50-64, that have not been vaccinated/ do not intend to get vaccinated (%)", size(med)) 

		
graph save "${output}/no_vax_a50.gph", replace


spmap no_vax_a65 using "${map}/SHARE_map.dta",id (_ID) clmethod(custom) clnumber(8) clbreaks(0 .1 .2 .3 .4 .5 .6 .7 .72) ///
	 fcolor (OrRd) ndfcolor(white) legend(label(1 "no data") label(2 "0-10%") label(3 "10-20%" ) label(4 "20-30%") label(5 "30-40%") lab(6 "40-50") lab(7 "50-60%") lab(8 "60-70%") lab(9 "over 70%")) ///
	legend(pos(10)) /// 
		legend(size(1.7)) title ("Respondents, aged 65-79, that have not been vaccinated/ do not intend to get vaccinated (%)", size(med)) 

		
graph save "${output}/no_vax_a65.gph", replace


spmap no_vax_a80 using "${map}/SHARE_map.dta",id (_ID) clmethod(custom) clnumber(8) clbreaks(0 .1 .2 .3 .4 .5 .6 .7 .72) ///
	 fcolor (OrRd) ndfcolor(white) legend(label(1 "no data") label(2 "0-10%") label(3 "10-20%" ) label(4 "20-30%") label(5 "30-40%") lab(6 "40-50") lab(7 "50-60%") lab(8 "60-70%") lab(9 "over 70%")) ///
	legend(pos(10)) /// 
		legend(size(1.7)) title ("Respondents, aged 80 and above, that have not been vaccinated/ do not intend to get vaccinated (%)", size(med)) 

		
graph save "${output}/no_vax_a80.gph", replace


spmap no_vax_male using "${map}/SHARE_map.dta",id (_ID) clmethod(custom) clnumber(8) clbreaks(0 .1 .2 .3 .4 .5 .6 .7 .72) ///
	 fcolor (OrRd) ndfcolor(white) legend(label(1 "no data") label(2 "0-10%") label(3 "10-20%" ) label(4 "20-30%") label(5 "30-40%") lab(6 "40-50") lab(7 "50-60%") lab(8 "60-70%") lab(9 "over 70%")) ///
	legend(pos(10)) /// 
		legend(size(1.7)) title (" Male respondents that have not been vaccinated/ do not intend to get vaccinated (%)", size(med)) 


graph save "${output}/no_vax_male.gph", replace
		
		
		
spmap no_vax_female using "${map}/SHARE_map.dta",id (_ID) clmethod(custom) clnumber(8) clbreaks(0 .1 .2 .3 .4 .5 .6 .7 .72) ///
	 fcolor (OrRd) ndfcolor(white) legend(label(1 "no data") label(2 "0-10%") label(3 "10-20%" ) label(4 "20-30%") label(5 "30-40%") lab(6 "40-50") lab(7 "50-60%") lab(8 "60-70%") lab(9 "over 70%")) ///
	legend(pos(10)) /// 
		legend(size(1.7)) title (" Female respondents that have not been vaccinated/ do not intend to get vaccinated (%)", size(med)) 


graph save "${output}/no_vax_female.gph", replace
*/
* Graphs: Aspect Ratios and Export

graph use "${output}/novax.gph" 
graph play "${output}/aspect_ratios.grec"
graph save "${output}/novax_all.gph", replace
graph export "${output}/novax_all.png", width(13000) height(10000) replace

/*
graph use "${output}\no_vax_a50.gph" 
graph play "${output}\aspect_ratios.grec"
graph save "${output}/no_vax_a50.gph", replace
graph export "${output}/no_vax_a50.png", as(png) replace

graph use "${output}\no_vax_a65.gph" 
graph play "${output}\aspect_ratios.grec"
graph save "${output}/no_vax_a65.gph", replace
graph export "${output}/no_vax_a65.png", as(png) replace

graph use "${output}\no_vax_a80.gph" 
graph play "${output}\aspect_ratios.grec"
graph save "${output}/no_vax_a80.gph", replace
graph export "${output}/no_vax_a80.png", as(png) replace

graph use "${output}\no_vax_male.gph" 
graph play "${output}\aspect_ratios.grec"
graph save "${output}/no_vax_male.gph", replace
graph export "${output}/no_vax_male.png", as(png) replace

graph use "${output}\no_vax_female.gph" 
graph play "${output}\aspect_ratios.grec"
graph save "${output}/no_vax_female.gph", replace
graph export "${output}/no_vax_female.png", as(png) replace

