*Author: Michael Bergmann
*Last updated: Sep 2021
*Source: Release 0 w91 & previous CAPI waves

********************************************************************************

*----[ O v e r v i e w  o f  C o n t e n t s ]---------------------------------

*** 1) Data preparation: CAPI information from previous waves
*** 2) Data preparation: CATI information from SCS1
*** 3) Data preparation: CATI information from SCS2



********************************************************************************
*** 1) Data preparation: CAPI information from previous waves
********************************************************************************

use "${data}\Vaccinations.dta", clear

gen samp = 1

*** Merge education and urban/rural from previous CAPI waves
merge 1:1 mergeid using "${w8}/sharew8_rel1-0-0_cv_r.dta", ///
	keepus(hhid8 coupleid8 country age2020 yrbirth cvresp deceased relrpers partnerinhh hhsize) nogen keep(1 3)
merge 1:1 mergeid using "${w8}/sharew8_rel1-0-0_gv_isced.dta", ///
	keepus(isced1997_r) nogen keep(1 3)
merge 1:1 mergeid using "${w8}/sharew8_rel1-0-0_ho.dta", ///
	keepus(ho037_) nogen keep(1 3)
merge 1:1 mergeid using "${w8}/sharew8_rel1-0-0_dn.dta", ///
	keepus(dn004_) nogen keep(1 3)
merge m:1 country using "${w8}/sharew8_rel1-0-0_gv_exrates.dta", ///
	keepus(nomx2020 pppk2019) nogen keep(1 3)


** Educational level
numlabel isced, add 
tab isced1997_r, m
rename isced1997_r educ	

preserve
	use "${wX}/sharewX_rel7-1-0_gv_allwaves_cv_r.dta", clear
		keep mergeid waveid_hh waveid
	foreach w of numlist 1 2 4 5 6 7 { 
		merge 1:1 mergeid using ${w`w'}/sharew`w'_rel7-1-0_gv_isced.dta, ///
			keepus(isced1997_r)
			rename isced1997_r isced1997_r_w`w'
			assert _merge!=2
			drop _merge
	}
	gen isced1997_r = isced1997_r_w1
	replace isced1997_r = isced1997_r_w2 if isced1997_r_w2!=.
	replace isced1997_r = isced1997_r_w4 if isced1997_r_w4!=.
	replace isced1997_r = isced1997_r_w5 if isced1997_r_w5!=.
	replace isced1997_r = isced1997_r_w6 if isced1997_r_w6!=.
	replace isced1997_r = isced1997_r_w7 if isced1997_r_w7!=.
	tab isced1997_r, m
	keep mergeid waveid_hh isced1997_r
	save "${data}/isced_w17.dta", replace
restore

merge 1:1 mergeid using ${data}/isced_w17.dta, nogen keep(1 3)
	
list mergeid waveid_hh educ isced1997_r in 1/100
tab educ isced1997_r, m
replace educ = isced1997_r if educ==.
tab educ, m
recode educ (-2=.b)(-1=.a)(0/2=1)(3 95 97=2)(4/6=3)	// 95+97: set to middle cat.
lab var educ "Education"
lab def educ 1 "primary" 2 "secondary" 3 "post-secondary", replace
lab val educ educ
numlabel educ, add
tab educ, m
drop isced1997_r

tab educ, gen(educ)
tab1 educ?, m


** Urban/rural
numlabel arealiving, add 
tab ho037_, m
rename ho037_ area
order area, last

preserve
	use "${wX}/sharewX_rel7-1-0_gv_allwaves_cv_r.dta", clear
		keep mergeid waveid_hh waveid
	foreach w of numlist 1 2 4 5 6 7 { 
		merge 1:1 mergeid using ${w`w'}/sharew`w'_rel7-1-0_gv_housing.dta, ///
			keepus(areabldgi)
			rename areabldgi areabldgi_w`w'
			assert _merge!=2
			drop _merge
	}
	gen areabldgi = areabldgi_w1
	replace areabldgi = areabldgi_w2 if areabldgi_w2!=.
	replace areabldgi = areabldgi_w4 if areabldgi_w4!=.
	replace areabldgi = areabldgi_w5 if areabldgi_w5!=.
	replace areabldgi = areabldgi_w6 if areabldgi_w6!=.
	replace areabldgi = areabldgi_w7 if areabldgi_w7!=.
	tab areabldgi, m
	keep mergeid waveid_hh areabldgi
	save "${data}/areabldgi_w17.dta", replace
restore

merge 1:1 mergeid using ${data}/areabldgi_w17.dta, nogen keep(1 3)
	
list mergeid waveid_hh area areabldgi in 1/100
tab area areabldgi, m
replace area = areabldgi if area==.
tab area, m

tab area caho037_, m
replace area = caho037_ if caho037_>=1 & caho037_<=5 // replace info with SCS2!
tab area, m
recode area (-2=.b)(-1=.a)(1=5)(2=4)(3=3)(4=2)(5=1)
lab def area 1 "rural area/village" 2 "small town" 3 "large town" ///
	4 "suburbs/outskirts of a big city" 5 "big city", replace
lab val area area
numlabel area, add
tab area, m
recode area (1/2=0)(3/5=1), gen(urban)
lab var urban "Urbanization"
lab def urban 0 "rural" 1 "urban", replace
lab val urban urban
numlabel urban, add
tab area urban, m
tab urban, m
drop areabldgi

tab urban, gen(urban)
tab1 urban?, m


** Household size
tab hhsize partnerinhh, m
recode hhsize (3/max=3), gen(hhcomp)
tab hhsize hhcomp, m
lab var hhcomp "Household composition"
lab def hhcomp 1 "single hh" 2 "2 person hh" 3 ">2 person hh"
lab val hhcomp hhcomp
numlabel hhcomp, add
tab hhcomp, m

recode hhcomp (1=0)(2 3=1), gen(hhcomp2)
tab hhcomp hhcomp2, m
tab hhcomp2, m


** Nursinghome respondents
numlabel yesno2, add
tab nursinghome, m


** Migration
tab dn004_, m
rename dn004 born	

preserve
	use "${wX}/sharewX_rel7-1-0_gv_allwaves_cv_r.dta", clear
		keep mergeid waveid_hh waveid
	foreach w of numlist 1 2 4 5 6 7 { 
		merge 1:1 mergeid using ${w`w'}/sharew`w'_rel7-1-0_dn.dta, ///
			keepus(dn004_)
			rename dn004_ dn004_w`w'
			assert _merge!=2
			drop _merge
	}
	gen dn004_ = dn004_w1
	replace dn004_ = dn004_w2 if dn004_w2!=.
	replace dn004_ = dn004_w4 if dn004_w4!=.
	replace dn004_ = dn004_w5 if dn004_w5!=.
	replace dn004_ = dn004_w6 if dn004_w6!=.
	replace dn004_ = dn004_w7 if dn004_w7!=.
	tab dn004_, m
	keep mergeid waveid_hh dn004_
	save "${data}/born_w17.dta", replace
restore

merge 1:1 mergeid using ${data}/born_w17.dta, nogen keep(1 3)
	
list mergeid waveid_hh born dn004_ in 1/100
tab born dn004_, m
replace born = dn004_ if born==.
tab born, m
recode born (-2=.b)(-1=.a)(1=0)(5=1)
lab var born "Born abroad"
lab def born 0 "No" 1 "Yes", replace
lab val born born
numlabel born, add
tab born, m
drop dn004_


save "${data}/Vaccinations_CAPI.dta", replace



********************************************************************************
*** 2) Data preparation: CATI information from SCS1
********************************************************************************

*** Generate equivalence weight per hh 	// new (MB): 28.10.2021
use "${scs2}/sharew91_rel0_cv_r.dta", clear
	keep mergeid hhid91 age2021 yrbirth interview

numlabel _all, add
tab yrbirth, m
mvdecode yrbirth, mv (-2=.b\-1=.a)
tab yrbirth, m
gen age = 2021-yrbirth
tab age, m

tab age2021, m

gsort hhid91 -age2021 mergeid
by hhid91: gen equivwt = 1 if _n == 1
by hhid91: replace equivwt = .5 if age2021 >= 14 & age != . & _n != 1
by hhid91: replace equivwt = .3 if age2021 >= 0 & age < 14 & _n != 1
by hhid91: replace equivwt = .5 if (age2021<0) & _n != 1
tab age equivwt, m nol
bysort hhid91: egen equivwt_hh = total(equivwt)
tab equivwt_hh, m

keep mergeid equivwt_hh

save "${data}/equivwt_hh_w8.dta", replace



*** Merge income & make ends meet from SCS1
use "${scs1}/sharew8_rel1-0-0_ca.dta", clear
	keep mergeid cahh017e caco007_
append using "${scs1}/sharew8_rel1-0-0_ca_at.dta", ///
	keep(mergeid cahh017e caco007_)

merge 1:1 mergeid using "${data}/Vaccinations_CAPI.dta"
// keep _merge==1 for imputation within household
merge 1:1 mergeid using "${data}/equivwt_hh_w8.dta", nogen keep(1 3)

order cahh017e caco007_, last
gen hhid_fix = substr(mergeid,1,9)
tab cahh017e, m
sort mergeid
list mergeid cahh017e in 1/100, sepby(hhid_fix)
bysort hhid_fix (mergeid): egen help = max(cahh017e)
list mergeid cahh017e help in 1/100, sepby(hhid_fix)
replace cahh017e = help if (cahh017e==. | cahh017e<0) & help!=.
recode cahh017e (-9999999=.)(-9999992=.b)(-9999991=.a), gen(hhinc)
tab hhinc if hhinc<10, m
recode hhinc (min/1=1)
tab hhinc, m
table country, c(mean hhinc) row
bysort country: sum hhinc
sum hhinc, d
tab hhinc hhsize if hhinc>=99999 & hhinc<., m
*replace hhinc = .d if hhinc>=99999 & hhinc<.	
// only for unrealistic high values (n=17)
sum hhinc
table country, c(mean hhinc) row
bysort country: sum hhinc
bysort country: tab hhinc
tab hhinc, m
drop help

gen hhinc_ppp = hhinc*nomx2020/pppk2019	// update with more recent information
replace hhinc_ppp = hhinc if hhinc>=.
tab hhinc_ppp, m
table country, c(mean hhinc_ppp) row

levelsof country, local(cnt)
foreach c of local cnt {
	qui sum hhinc_ppp if country==`c', d
	recode hhinc_ppp (min/`r(p25)'=1)(`r(p25)'/`r(p50)'=2)(`r(p50)'/`r(p75)'=3) ///
		(`r(p75)'/max=4)(.a=5)(.b=6) if country==`c', gen(hhinc2_`c')
}
egen hhinc2 = rowtotal(hhinc2_??)
list mergeid cahh017e hhinc hhinc2 in 1/100, sepby(hhid_fix)
bysort hhid_fix (mergeid): egen help = max(hhinc2)
list mergeid cahh017e hhinc hhinc2 help in 1/100, sepby(hhid_fix)
replace hhinc2 = help if hhinc>=. & help!=.
tab hhinc2, m
replace hhinc2 = . if hhinc2==0
lab var hhinc2 "Total household income (cat.)"
lab def hhinc2 1 "1st quartile" 2 "2nd quartile" 3 "3rd quartile" ///
	4 "4th quartile" 5 "dk" 6 "rf", replace
lab val hhinc2 hhinc2
numlabel hhinc2, add
tab hhinc2, m
drop hhinc2_??
drop help

recode hhinc2 (5/6=5)
lab def hhinc2 1 "1st quartile" 2 "2nd quartile" 3 "3rd quartile" ///
	4 "4th quartile" 5 "dk+rf", replace
lab val hhinc2 hhinc2
numlabel hhinc2, add
tab hhinc2, m
 

** Equivalised disposable income	// new (MB): 20.10.2021
tab hhsize, m
recode hhsize (2=1.5)(3=2)(4=2.5)(5=3)(6=3.5)(7=4)(8=4.5)(9=5)(10=5.5)(11=6), ///
	gen(hhsize_eq)
tab hhsize_eq, m
tab equivwt_hh hhsize_eq, m
pwcorr equivwt_hh hhsize_eq	// .98!
gen inc = hhinc/equivwt_hh
replace inc = .a if hhinc==.a
replace inc = .b if hhinc==.b
tab inc, m

levelsof country, local(cnt)
foreach c of local cnt {
	qui sum inc if country==`c', d
	gen arm_`c' = `r(p50)'*.6 if country==`c'
	gen inc2_`c' = 1 if inc<=arm_`c' & country==`c'
	replace inc2_`c' = 0 if inc>arm_`c' & country==`c'
	replace inc2_`c' = 9 if inc>. & country==`c'
	replace inc2_`c' = . if inc==. & country==`c'
}
egen arm = rowtotal(arm_??)
tab arm, m
replace arm = . if arm==0
egen inc2 = rowtotal(inc2_??)
tab inc2, m
replace inc2 = inc if inc>=.
list mergeid hhinc hhsize inc arm inc2 in 1/100 if _merge==3, sepby(hhid_fix)
replace inc2 = 9 if inc2>.
tab inc2, m
lab var inc2 "<=60% of equivalised disposable income"
lab def inc2 0" no" 1 "yes" 9 "dk/rf", replace
lab val inc2 inc2
numlabel inc2, add
tab inc2, m
drop inc2_?? arm_??
clonevar inc3 = inc2
tab inc3, m

tab inc3, gen(inc3)
tab1 inc3?, m

recode inc2 (9=.a)
tab inc2, m

keep if samp==1
// drop _merge==2
drop _merge


save "${data}/Vaccinations_CAPI+SCS1.dta", replace



********************************************************************************
*** 3) Data preparation: CATI information from SCS2
********************************************************************************

use "${data}/Vaccinations_CAPI+SCS1.dta", clear

** Working status
tab caep005_, m
recode caep005_ (-2=.b)(-1=.a)(3/max=3), gen(empl)
tab caep005_ empl, m
lab def empl 1 "retired" 2 "employed/self-employed" 3 "other", replace
lab val empl empl
numlabel empl, add
tab empl, m

tab empl, gen(empl)
tab1 empl?, m


tab caep005_, m
recode caep005_ (-2=.b)(-1=.a)(4 5 97=4), gen(empl_new)
tab caep005_ empl_new, m
lab def empl_new 1 "retired" 2 "employed/self-employed" 3 "unemployed" ///
	4 "other non-working state", replace
lab val empl_new empl_new
numlabel empl_new, add
tab empl_new, m

tab empl_new, gen(empl_new)
tab1 empl_new?, m


** Make ends meet
tab caco107_, m
mvdecode caco007_, mv(-2=.b\-1=.a\-9=.c)
tab caco007_, m

sort mergeid
list mergeid caco007_ in 1/100, sepby(hhid_fix)
bysort hhid_fix (mergeid): egen help = max(caco007_)
list mergeid caco007_ help in 1/100, sepby(hhid_fix)
gen makeend = caco007_
replace makeend = help if caco007_>=. & help!=.
list mergeid makeend help in 1/100, sepby(hhid_fix)
lab var makeend "Make ends meet"
lab def makeend 1 "With great difficulty" 2 "With some difficulty" ///
	3 "Fairly easily" 4 "Easily"
lab val makeend makeend
numlabel makeend makeend, add
tab makeend, m
drop help

tab makeend, gen(make)
tab1 make?

recode makeend (1 2=1)(3 4=0), gen(makeend_new)
tab makeend makeend_new, m
lab var makeend_new "Make ends meet"
lab def makeend_new 0 "Fairly easily/Easily" 1 "With great/some difficulty"
lab val makeend_new makeend_new
numlabel makeend_new makeend_new, add
tab makeend_new, m

tab makeend_new, gen(makeend_new)
tab1 makeend_new?, m


** Self-rated health
tab caph003_, m
recode caph003_ (-2=.b)(-1=.a)(5=0)(4=1)(3=2)(2=3)(1=4), gen(health)
tab caph003_ health, m
lab def health 0 "poor" 1 "fair" 2 "good" 3 "very good" 4 "excellent", replace
lab val health health
numlabel health, add
tab health, m

recode health (0/1=0)(2=1)(3 4=2), gen(health3)
tab health health3, m
lab var health3 "Self-rated health (poor/fair & very good/excellent collapsed)"
lab def health3 0 "poor/fair" 1 "good" 2 "very good/excellent", replace
lab val health3 health3
numlabel health3, add
tab health3, m

tab health3, gen(health3)
tab1 health3?, m


** GALI
tab caph105_, m
recode caph105_ (-2=.b)(-1=.a)(1=2)(2=1)(3=0), gen(gali)	// dk set to 1!
tab caph105_ gali, m
lab def gali 0 "not limited" 1 "limited, but not severely" 2 "severely limited"
lab val gali gali
numlabel gali, add
tab gali, m

recode gali (1/2=1), gen(gali2)
tab gali gali2, m
lab var gali2 "GALI (dichotomized)"
tab gali2, m


** Diseases (without hip fracture!)
tab1 cah004_?, m
recode cah004_? (-2=.b)(-1=.a)(5=0)
egen disease = rowtotal(cah004_2 cah004_3 cah004_4 cah004_5 cah004_6 cah004_7)
egen mis = rowmiss(cah004_2 cah004_3 cah004_4 cah004_5 cah004_6 cah004_7)
tab mis, m
replace disease = . if mis==6
list cah004_? disease in 1/100
lab var disease "Number of diseases"
tab disease, m

recode disease (1/max=1), gen(disease2)
tab disease disease2, m
lab var disease2 "Diseases (dichotomized)"
tab disease2, m
drop mis

tab disease2, gen(disease2)
tab1 disease2?, m


** Mental health issues
tab camh002_, m
recode camh002_ (-2=.b)(-1=1)(5=0), gen(depres)	// dk set to 1!
tab camh002_ depres, m
tab depres, m

tab camh113_1, m
recode camh113_1 (-2=.b)(1=2)(2=1)(3 -1 -9=0), gen(depres_change) // dk set to 0!
tab camh113_1 depres_change, m
lab var depres_change "Depression change"
lab def change 0 "about the same" 1 "less" 2 "more", replace
lab val depres_change change
numlabel change, add
tab depres_change, m

tab cah020_, m
recode cah020_ (-2=.b)(-1=1)(5=0), gen(anx)	// dk set to 1!
tab cah020_ anx, m
tab anx, m

tab1 cah121_1, m
recode cah121_1 (-2=.b)(1=2)(2=1)(3 -1 -9=0), gen(anx_change)	// na set to 0!
tab cah121_1 anx_change, m
lab var anx_change "Anxiety change"
lab val anx_change change
numlabel change, add
tab anx_change, m

tab camh007_, m
recode camh007_ (-2=.b)(-1=1)(2=0), gen(sleep)	// dk set to 1!
tab camh007_ sleep, m
tab sleep, m

tab1 camh118_1, m
recode camh118_1 (-2=.b)(1=2)(2=1)(3 -1 -9=0), gen(sleep_change)	// na set to 0!
tab camh118_1 sleep_change, m
lab var sleep_change "Sleep change"
lab val sleep_change change
numlabel change, add
tab sleep_change, m

tab camh037_, m
recode camh037_ (-2=.b)(1=2)(-1 2=1)(3=0), gen(lonely)	// dk set to 1!
tab camh037_ lonely, m
lab def lonely 0 "hardly ever/never" 1 "sometimes" 2 "often"
lab val lonely lonely
numlabel lonely, add
tab lonely, m

recode lonely (2=1), gen(lonely2)
tab lonely lonely2, m

tab1 camh148_, m
recode camh148_ (-2=.b)(1=2)(2=1)(3 -1 -9=0), gen(lonely_change)	// na set to 0!
tab camh148_ lonely_change, m
lab var lonely_change "Loneliness change"
lab val lonely_change change
numlabel change, add
tab lonely_change, m

egen mental = rowtotal(depres anx sleep lonely2)
egen mis = rowmiss(depres anx sleep lonely2)
tab mis, m
replace mental = . if mis==4
list depres anx sleep lonely2 mental in 1/100
lab var mental "Having mental health issues"
tab mental, m

recode mental (1/max=1), gen(mental2)
lab var mental2 "Having mental health issues (dichotomized)"
tab mental2, m
drop mis

tab mental2, gen(mental2)
tab1 mental2?, m


** Affection
tab cac102_, m
recode cac102_ (-2=.b)(-1=.a)(5=0), gen(symp)
tab cac102_ symp, m
tab symp, m

tab symp cac103_1, m
gen symp_r = .
replace symp_r = 0 if symp==0
replace symp_r = 0 if cac103_1==0
replace symp_r = 1 if cac103_1==1
lab var symp_r "COVID-19: Respondent had symptoms"
tab symp_r, m

tab cac104_,m
recode cac104_ (-2=.b)(-1=.a)(5=0), gen(test_pos)
tab cac104_ test_pos, m
tab test_pos, m

tab test_pos cac105_1, m
gen test_pos_r = .
replace test_pos_r = 0 if test_pos==0
replace test_pos_r = 0 if cac105_1==0
replace test_pos_r = 1 if cac105_1==1
lab var symp_r "COVID-19: Respondent tested positive"
tab test_pos_r, m

tab cac110_, m
recode cac110_ (-2=.b)(-1=.a)(5=0), gen(hosp)
tab cac110_ hosp, m
tab hosp, m

tab hosp cac111_1, m
gen hosp_r = .
replace hosp_r = 0 if hosp==0
replace hosp_r = 0 if cac111_1==0
replace hosp_r = 1 if cac111_1==1
lab var symp_r "COVID-19: Respondent hospitalized"
tab hosp_r, m

tab cac113_, m
recode cac113_ (-2=.b)(-1=.a)(5=0), gen(died)
tab cac113_ died, m
tab died, m

* others
tab1 symp test_pos hosp died, m
gen affected = 0
replace affected = 1 if symp==1 | test_pos==1 
replace affected = 2 if hosp==1 | died==1
egen mis = rowmiss(symp test_pos hosp died)
tab mis, m
list symp test_pos hosp died mis in 1/100
replace affected = . if mis==4
lab var affected "Knowing s.o. affected by Covid-19"
lab def affected 0 "not affected" 1 "mildly affected" 2 "severely affected"
lab val affected affected
numlabel affected, add
tab affected, m

tab affected, gen(affected)
tab1 affected?, m

* self
tab1 symp_r test_pos_r hosp_r, m
gen affected_r = 0
replace affected_r = 1 if symp_r==1 | test_pos_r==1
replace affected_r = 2 if hosp_r==1
egen mis_r = rowmiss(symp_r test_pos_r hosp_r)
tab mis_r, m
list symp_r test_pos_r hosp_r mis in 1/100
replace affected_r = . if mis_r==3
lab var affected_r "Being affected from Covid-19"
lab def affected_r 0 "not affected" 1 "mildly affected" 2 "severely affected"
lab val affected_r affected_r
numlabel affected_r, add
tab affected_r, m
drop mis mis_r

tab affected_r, gen(affected_r)
tab1 affected_r?, m


** Previous vaccinations
tab1 cahc884_ cahc119_, m
recode cahc884_ (5=0)(-2=.b)(-1=.a), gen(flu)
tab cahc884_ flu, m
tab flu, m

recode cahc119_ (5=0)(-2=.b)(-1=.a), gen(pneum)
tab cahc119_ pneum, m
tab pneum, m


** internet use
tab cait104_, m
recode cait104_ (5=0)(-2=.b)(-1=.a), gen(internet)
tab cait104_ internet, m
tab internet, m

tab cait106_3, m
recode cait106_3 (-2=.b)(1=3)(2 -1=2)(3=1)(4 -9=0), gen(internet_change)	// na set to 1!
tab cait106_3 internet_change, m
lab var internet_change "Internet use change"
lab def change 0 "not at all" 1 " less" 2 "about the same" 3 "more", replace
lab val internet_change change
numlabel change, add
tab internet_change, m


** social contact
tab1 cas103_?, m
local n =1
foreach var of varlist cas103_? {
	recode `var' (-2=.b)(-1=.a)(1=4)(2=3)(3=2)(4=1)(5 99=0), gen(contact`n')
	local n = `n'+1
}
sum contact?
egen contacts = rowtotal(contact?)
egen mis = rowmiss(contact?)
tab mis, m
replace contacts = . if mis==5
list contact* in 1/200
tab contacts, m
sum contacts, d
recode contacts (min/6=0)(7/max=1), gen(contacts2)
tab contacts contacts2, m
tab contacts2, m


**  Region
tab country, m
gen region = ""
replace region = "Baltic" ///
	if country==35 | country==48 | country==57
replace region = "North" ///
	if country==13 | country==18 | country==55
replace region = "South" ///
	if country==15 | country==16 | country==19 | country==25 | country==33 | ///
		country==34 | country==47 | country==53 | country==59
replace region = "West" ///
	if country==11 | country==12 | country==14 | ///
		country==17 | country==20 | country==23 | ///
		country==31
replace region = "East" ///
	if country==28 | country==29 | country==32 | ///
		country==51 | country==61 | country==63
tab country region, m
lab var region "Region"
tab region, m

encode region, gen(region2) lab(region)
lab var region2 "Region"
numlabel region, add
tab region2, m
recode region2 (3=1)(5=2)(4=3)(2=4)(1=5)
lab def region 1 "North" 2 "West" 3 "South" 4 "East" 5 "Baltic", replace
lab val region2 region
numlabel region, add
tab region2, m
tab region region2, m
tab country region2, m


save "${data}/Vaccinations_Analyses.dta", replace



