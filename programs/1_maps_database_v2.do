* Create Database to be used for Maps:

* Create new variable:

recode vax2 (1 2 = 0) (3 4 = 1), generate(no_vax)

preserve
collapse (mean) no_vax, by(country)
save  "${temp}\no_vax_all.dta", replace
restore

preserve
collapse (mean) no_vax  if agecat == 1, by(country)
gen no_vax_a50 = no_vax*1
save  "${temp}\no_vax_a50.dta", replace
restore


preserve
collapse (mean)no_vax if agecat == 2, by(country)
gen no_vax_a65 = no_vax*1
save  "${temp}\no_vax_a65.dta", replace
restore

preserve
collapse (mean) no_vax  if agecat == 3, by(country)
gen no_vax_a80 = no_vax*1
save  "${temp}\no_vax_a80.dta", replace
restore

preserve
collapse (mean) no_vax  if female == 1, by(country)
gen no_vax_female = no_vax*1
save  "${temp}\no_vax_female.dta", replace
restore

preserve
collapse (mean) no_vax  if female == 0, by(country)
gen no_vax_male = no_vax*1
save  "${temp}\no_vax_male.dta", replace
restore

use  "${temp}\no_vax_all.dta", clear
merge 1:1 country using "${temp}\no_vax_a50.dta"
drop _merge
merge 1:1 country using "${temp}\no_vax_a65.dta"
drop _merge
merge 1:1 country using "${temp}\no_vax_a80.dta"
drop _merge
merge 1:1 country using "${temp}\no_vax_male.dta"
drop _merge
merge 1:1 country using "${temp}\no_vax_female.dta"
drop _merge


clonevar country0 = country
decode country0, gen(country4)

save "${map}\Vaccinations_maps.dta", replace


 
 **** Create Map Database for further processing
  
** Shape data UIA World Countries Boundaries can be found here:
** https://hub.arcgis.com/datasets/UIA::uia-world-countries-boundaries/explore?location=-0.010518%2C0.000000%2C0.94
 cap shp2dta using "${world}/World_Countries__Generalized_.shp",	data(world_data) coor(world_corr) genid(_ID) gencentroids(c) replace	// Stata 14
// ignore error message!

cd "${world}"
use world_corr
scatter _Y _X, msize(large) msymbol(point)

merge m:1 _ID using world_data
assert _merge==3

tab COUNTRY, m
keep if COUNTRY== "Austria" | COUNTRY == "Belgium" | COUNTRY == "Bulgaria" | ///
	COUNTRY == "Switzerland" | COUNTRY == "Czech Republic" | COUNTRY == "Cyprus" | ///
	COUNTRY == "Denmark" | COUNTRY == "Germany" | COUNTRY == "Estonia" | ///
	COUNTRY == "Latvia" | COUNTRY == "Lithuania" | COUNTRY == "Spain" | ///
	COUNTRY == "Finland" | COUNTRY == "France" | COUNTRY == "Portugal" | ///
	COUNTRY == "Israel" | COUNTRY == "Greece" | COUNTRY == "Hungary" | ///
	COUNTRY == "Croatia" | COUNTRY == "Italy" | COUNTRY == "Netherlands" | ///
	COUNTRY == "Poland" | COUNTRY == "Romania" | COUNTRY == "Slovakia" | ///
	COUNTRY == "Malta" | COUNTRY == "Slovenia" | COUNTRY == "Sweden" | ///
	COUNTRY == "Luxembourg" | COUNTRY == "Ireland" | COUNTRY == "United Kingdom" | ///
	COUNTRY == "Ukraine" | COUNTRY == "Belarus" | COUNTRY == "Moldova" | ///
	COUNTRY == "Norway" | COUNTRY == "Albania" | COUNTRY == "Bosnia and Herzegovina" | ///
	COUNTRY == "Serbia" | COUNTRY == "Montenegro" | COUNTRY == "North Macedonia"
tab COUNTRY, m
rename COUNTRY country4

scatter _Y _X, msize(tiny) msymbol(point)


sort _ID
keep if _X > -20	// drop Portuguese islands
scatter _Y _X,  aspect(1) msize(tiny) msymbol(point)


drop FID COUNTRYAFF AFF_ISO _merge

save "${map}/SHARE_map.dta", replace

use "${world}\world_data.dta", clear

*** keep SHARE countries
keep if COUNTRY== "Austria" | COUNTRY == "Belgium" | COUNTRY == "Bulgaria" | ///
	COUNTRY == "Switzerland" | COUNTRY == "Czech Republic" | COUNTRY == "Cyprus" | ///
	COUNTRY == "Denmark" | COUNTRY == "Germany" | COUNTRY == "Estonia" | ///
	COUNTRY == "Latvia" | COUNTRY == "Lithuania" | COUNTRY == "Spain" | ///
	COUNTRY == "Finland" | COUNTRY == "France" | COUNTRY == "Portugal" | ///
	COUNTRY == "Israel" | COUNTRY == "Greece" | COUNTRY == "Hungary" | ///
	COUNTRY == "Croatia" | COUNTRY == "Italy" | COUNTRY == "Netherlands" | ///
	COUNTRY == "Poland" | COUNTRY == "Romania" | COUNTRY == "Slovakia" | ///
	COUNTRY == "Malta" | COUNTRY == "Slovenia" | COUNTRY == "Sweden" | ///
	COUNTRY == "Luxembourg" | COUNTRY == "Ireland" | COUNTRY == "United Kingdom" | ///
	COUNTRY == "Ukraine" | COUNTRY == "Belarus" | COUNTRY == "Moldova" | ///
	COUNTRY == "Norway" | COUNTRY == "Albania" | COUNTRY == "Bosnia and Herzegovina" | ///
	COUNTRY == "Serbia" | COUNTRY == "Montenegro" | COUNTRY == "North Macedonia"
tab COUNTRY, m 

rename COUNTRY country4
order _ID, first
drop FID COUNTRYAFF AFF_ISO


*** create proper map
spmap using "${map}/SHARE_map.dta", id(_ID)


merge m:1 country4 using "${map}\Vaccinations_maps.dta"
