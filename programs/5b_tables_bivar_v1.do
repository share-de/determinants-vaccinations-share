*Author: Arne Bethmann
*Last updated: Nov 2021
*Source: Release 0 w91 & previous CAPI waves

********************************************************************************
*** Bi-variate tables for publication using asdoc RTF output
********************************************************************************
clear all
global data "R:\Research_projects\2_SHARE_Covid_Research\_Data\Vaccination\Rel0"
global output "S:\Research_projects\Covid_Research\Vaccination\Determinants\graphs\Rel0"


use "${data}/Vaccinations_Analyses_countries.dta", clear


********************************************************************************
*** 1) Define program for standardized bi-variate table output
********************************************************************************
program vax_bivar_table
	syntax, ///
		variables(string)	 /// List variables that should be put into table
		title(string)		 /// asdoc String for title
		h1(string)			 /// asdoc String for heading 1
		h2(string)			 /// asdoc String for heading 2
		h3(string)			 /// asdoc String for heading 3
		outfile(string)		 /// Output file
		foot(string)         // String for footer

	qui{
	
	noi di ""
	noi di "Generating bi-variate table: `title'"
	* Add header rows
	local titlerow = "Proportion of vaccination hesitancy: " + "`title'"
	local hrow1 = "\i, " + "`h1'"
	local hrow2 = "\i, " + "`h2'"
	local hrow3 = "Country, " + "`h3'"

	asdoc, row(`hrow1') replace title(`titlerow') save(`outfile')
	asdoc, row(`hrow2') save(`outfile')
	asdoc, row(`hrow3') save(`outfile')	
	
	* Add each characteristic row by row by looping
	levelsof cnt, local(cnt)
	foreach c of local cnt {
		noi di "`c'"
	
		* IVS
		foreach iv of varlist  `variables' {
			qui logit vax_int_ny i.`iv' [pw=wgt] if cnt == "`c'", vce(robust)
			qui margins, over(`iv') post
			matrix m = r(table)
			qui levelsof `iv', local(levels)
			local i = 1
			foreach lev of local levels {
				local val = m[1,`i']*100
				asdoc, accum(`val')
				local val = m[5,`i']*100
				asdoc, accum(`val')
				local val = m[6,`i']*100
				asdoc, accum(`val')
				local i = `i' + 1
			}
		}
		qui asdoc, row(`c', $accum) dec(1) save(`outfile')
	}

	* Add total numbers
	foreach iv of varlist  `variables' {
		qui logit vax_int_ny i.`iv' [pw=wgt], vce(robust)
		qui margins, over(`iv') post
		matrix m = r(table)
		qui levelsof `iv', local(levels)
		local i = 1
		foreach lev of local levels {
			local val = m[1,`i']*100
			asdoc, accum(`val')
			local val = m[5,`i']*100
			asdoc, accum(`val')
			local val = m[6,`i']*100
			asdoc, accum(`val')
			local i = `i' + 1
		}
	}
	qui asdoc, row(Total, $accum) dec(1) save(`outfile')

	* Add footer row
	asdoc, text(`foot') save(`outfile')

	}
end


********************************************************************************
*** 2) Generate tables
********************************************************************************

** Define output file and footer for all tables bivariate tables
local foot = "Data: Preliminary SHARE wave 9 COVID-19 Survey 2 release 0 data."

** Bi-variate table socio-dempgraphics
vax_bivar_table, ///
	variables(agecat female educ) ///
	title("Socio-demographic determinants") ///
	h1("Age group, \i, \i, \i, \i, \i, \i, \i, \i, Gender, \i, \i, \i, \i, \i, Education, \i, \i, \i, \i, \i, \i, \i, \i") ///
	h2("50-64 years, \i, \i, 65-79 years, \i, \i, 80+ years, \i, \i, Male, \i, \i, Female, \i, \i, Primary, \i, \i, Secondary, \i, \i, Post-secondary, \i, \i") ///
	h3("%, CI, \i, %, CI, \i, %, CI, \i, %, CI, \i, %, CI, \i, %, CI, \i, %, CI, \i, %, CI, \i") ///
	outfile("$output/bivar_demo.doc") ///
	foot(`foot')

** Bi-variate table health
vax_bivar_table, ///
	variables(health3 disease2 mental2 affected) ///
	title("Health-related determinants") ///
	h1("Self-rated health, \i, \i, \i, \i, \i, \i, \i, \i, Diagnosed physical illness, \i, \i, \i, \i, \i, Mental health issues, \i, \i, \i, \i, \i, Affectedness by COVID-19, \i, \i, \i, \i, \i, \i, \i, \i") ///
	h2("Poor/fair, \i, \i, Good, \i,  \i, Very good/excellent, \i, \i, No, \i, \i, Yes, \i, \i, No, \i, \i, Yes, \i, \i, No, \i, \i, Mildly, \i, \i, Severely, \i, \i") ///
	h3("%, CI, \i, %, CI, \i, %, CI, \i, %, CI, \i, %, CI, \i, %, CI, \i, %, CI, \i, %, CI, \i, %, CI, \i, %, CI, \i") ///
	outfile("$output/bivar_health.doc") ///
	foot(`foot')

** Bi-variate table living conditions and economic situation
vax_bivar_table, ///
	variables(urban makeend_new inc2 empl) ///
	title("Determinants related to living conditions and economic situation") ///
	h1("Area of living, \i, \i, \i, \i, \i, Making ends meet, \i, \i, \i, \i, \i, At risk of poverty, \i, \i, \i, \i, \i, Working status, \i, \i, \i, \i, \i, \i, \i, \i") ///
	h2("Rural, \i, \i, Urban, \i, \i, Fairly easily/easily, \i, \i, With great/some difficulties, \i, \i, No, \i, \i, Yes, \i, \i, Retired, \i, \i, Employed/self-employed, \i, \i, Other non-working, \i, \i") ///
	h3("%, CI, \i, %, CI, \i, %, CI, \i, %, CI, \i, %, CI, \i, %, CI, \i, %, CI, \i, %, CI, \i, %, CI, \i") ///
	outfile("$output/bivar_living.doc") ///
	foot(`foot')

	