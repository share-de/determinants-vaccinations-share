*Author: Arne Bethmann
*Last updated: Nov 2021
*Source: Release 0 w91 & previous CAPI waves

********************************************************************************
*** Multi-variate tables for publication using asdoc RTF output
********************************************************************************
clear all
global data "R:\Research_projects\2_SHARE_Covid_Research\_Data\Vaccination\Rel0"
global output "S:\Research_projects\Covid_Research\Vaccination\Determinants\graphs\Rel0"


use "${data}/Vaccinations_Analyses_countries.dta", clear

********************************************************************************
** Original Model from analyses with centered dummies
********************************************************************************
logit vax_int_ny agecat2 agecat3 female educ2 educ3 /*internet*/ ///
	health32 health33 disease2 mental2 affected2 affected3 ///
	urban makeend_new inc32 inc33 empl2 empl3 ///
	i.country [pw=wgt], vce(robust)
*ssc install center, replace	
center agecat2 agecat3 female educ2 educ3 ///
	health32 health33 disease2 mental2 affected2 affected3 ///
	urban makeend_new inc32 inc33 empl2 empl3 [aw=wgt] if e(sample), st replace
*sum c_* [aw=wgt] if e(sample)

eststo clear
logit vax_int_ny c_agecat2 c_agecat3 c_female c_educ2 c_educ3 ///
	c_health32 c_health33 c_disease2 c_mental2 c_affected2 c_affected3 ///
	c_urban c_makeend_new c_inc32 c_inc33 c_empl2 c_empl3 ///
	i.country [pw=wgt], vce(robust)

label var vax_int_ny "Undecided/unwilling to receive vaccination"	
label var c_agecat2 "Age 65-79"
label var c_agecat3 "Age 80 and above"
label var c_female "Female"
label var c_educ2 "Secondary education"
label var c_educ3 "Post-secondary education"
label var c_health32 "Good health"
label var c_health33 "Very good/excellent health" 
label var c_disease2 "Diagnosed physical illness"
label var c_mental2 "Mental health issues"
label var c_affected2 "Mild COVID-19 case in vicinity"
label var c_affected3 "Severe COVID-19 case in vicinity"
label var c_urban "Living in urban area"
label var c_makeend_new "Difficult to make ends meet"
label var c_inc32 "At risk of poverty"
label var c_inc33 "No information on income"
label var c_empl2 "Employed/self-employed"
label var c_empl3 "Other employment status"

*esttab, ///	
*esttab using $output\Logit_vax_int_pub.rtf, replace ///
*	b(%8.3f) ci(%8.2f) label wide nonum nomtitles noomitted varwidth(45)

esttab using $output\Logit_vax_int_pub.rtf, replace ///
	b(%8.3f) p(%8.3f) label wide nonum nomtitles nostar noomitted varwidth(45)


/*
********************************************************************************
** 	Model using factor variable parametrization
********************************************************************************
logit vax_int_ny i.agecat i.female i.educ i.health3 i.disease2 i.mental2 ///
	i.affected i.urban i.makeend_new i.inc3 i.empl i.country [pw=wgt], ///
	vce(robust)

*esttab using $output\Logit_vax_int_fv_pub.rtf, replace ///
esttab, ///
	b(%8.3f) ci(%8.2f) label wide nonum nomtitles noomitted varwidth(45)


margins, dydx(*) post
*esttab using $output\Logit_vax_int_fv_pub.rtf, replace ///
esttab, ///
	b(%8.3f) ci(%8.2f) label wide nonum nomtitles noomitted varwidth(45)	
*/
