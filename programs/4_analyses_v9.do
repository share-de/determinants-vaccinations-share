*Author: Michael Bergmann
*Last updated: Sep 2021
*Source: Release 0 w91 & previous CAPI waves


********************************************************************************
*----[ O v e r v i e w  o f  C o n t e n t s ]----------------------------------

*** 1) Descriptive analyses
*** 2) Bivariate analyses of determinants
*** 3) Multivariate analyses of determinants
*** 4) Country differences
********************************************************************************



********************************************************************************
*** 1) Descriptive analyses
********************************************************************************

use "${data}/Vaccinations_Analyses.dta", clear


***********************
** a) Data preparations
***********************

* Operationalization of DV 
tab vax, m
tab vax2, m
tab vax2 vax, m
recode vax2 (1 2=1)(3 4=0), gen(vax_int_yn)
tab vax2 vax_int_yn, m
tab vax_int_yn, m

recode vax_int_yn (0=1)(1=0), gen(vax_int_ny)	// better for illustration
lab var vax_int_ny "Not vaccinated"
tab vax_int_ny, m

tab vax_int_ny vax_int3, m
gen vax_int_undec = .
replace vax_int_undec = 0 if vax_int_ny==0
replace vax_int_undec = 1 if vax_int_ny==1 & vax_int3==1
lab var vax_int_ny "Undecided"
tab vax_int_undec, m

tab vax_int_ny vax_int4, m
gen vax_int_no = .
replace vax_int_no = 0 if vax_int_ny==0
replace vax_int_no = 1 if vax_int_ny==1 & vax_int4==1
lab var vax_int_ny "Not willing"
tab vax_int_no, m

* Check distribution of respondents with an COVID-19 infection
tab vax2 test_pos_r, m col
// 65% with an infection are already vaccinated
// 16% want to be vaccinated
// 11% are still undecided
// 8% don't want to be vaccinated


* Check distribution across regions in CH
gen cnt_ch = .
replace cnt_ch = 1 if substr(mergeid,1,2)=="Cg"
replace cnt_ch = 2 if substr(mergeid,1,2)=="Cf"
replace cnt_ch = 3 if substr(mergeid,1,2)=="Ci"
tab cnt cnt_ch, m
table cnt_ch [aw=cciw_w91], ///
	c(mean vax_int1 mean vax_int2 mean vax_int3 mean vax_int4) row


* Trim weights to avoid extreme high values that lead to large SEs in preliminary data
cap drop wgt
table country, c(max cciw_w91) row format(%12.3f)
gen wgt = cciw_w91
sum wgt, d
tab wgt if wgt>r(p99), m
replace wgt = 150000 if wgt>150000 & wgt!=.	// 61 obs.
table country, c(max cciw_w91 max wgt) row format(%12.3f)

table cnt_ch [aw=wgt], ///
	c(mean vax_int1 mean vax_int2 mean vax_int3 mean vax_int4) row
	

* Country labels
tab country, m
gen cnt = ""
replace cnt = "Austria" if country == 11
replace cnt = "Germany" if country == 12
replace cnt = "Sweden" if country == 13
replace cnt = "Netherlands" if country == 14
replace cnt = "Spain" if country == 15
replace cnt = "Italy" if country == 16
replace cnt = "France" if country == 17
replace cnt = "Denmark" if country == 18
replace cnt = "Greece" if country == 19
replace cnt = "Switzerland" if country == 20
replace cnt = "Belgium" if country == 23
replace cnt = "Israel" if country == 25
replace cnt = "Czech Rep." if country == 28
replace cnt = "Poland" if country == 29
replace cnt = "Luxembourg" if country == 31
replace cnt = "Hungary" if country == 32
replace cnt = "Portugal"  if country == 33
replace cnt = "Slovenia" if country == 34
replace cnt = "Estonia" if country == 35
replace cnt = "Croatia" if country == 47
replace cnt = "Lithuania" if country == 48
replace cnt = "Bulgaria" if country == 51
replace cnt = "Cyprus" if country == 53
replace cnt = "Finland" if country == 55
replace cnt = "Latvia" if country == 57
replace cnt = "Malta" if country == 59
replace cnt = "Romania" if country == 61
replace cnt = "Slovakia" if country == 63
tab cnt, m


save "${data}/Vaccinations_Analyses_countries.dta", replace



************************
** b) Sample description
************************

* b1) Dependent variable
sum vax2 vax_int?

* Create heading for table: Vaccination
asdoc, row(Country, Number of respondents, Vaccinated in %, Willing to be vaccinated in %, Undecided in %, Unwilling to be vaccinated in %) ///
	replace title(Sample distribution: Vaccination & intention) ///
	save($output/Sample_desc_DV.doc)

* Add each characteristic row by row by looping
levelsof cnt, local(cnt)
foreach c of local cnt {
	
	noi di "`c'"
	
	* Number of respondents
	qui sum vax2 if cnt=="`c'" [aw=wgt]
	local m = `r(N)'
	asdoc, accum(`m')
	
	* Vaccinated
	qui sum vax_int1 if cnt=="`c'" [aw=wgt]
	local m = `r(mean)'*100
	asdoc, accum(`m')
	
	* Willing to be vaccinated
	qui sum vax_int2 if cnt=="`c'" [aw=wgt]
	local m = `r(mean)'*100
	asdoc, accum(`m')
	
	* Undecided
	qui sum vax_int3 if cnt=="`c'" [aw=wgt]
	cap local m = `r(mean)'*100
	asdoc, accum(`m')
	
	* Unwilling to be vaccinated
	qui sum vax_int4 if cnt=="`c'" [aw=wgt]
	local m = `r(mean)'*100
	asdoc, accum(`m')

	qui asdoc, row(`c', $accum) dec(1) save($output/Sample_desc_DV.doc)
}

* Add numbers
qui sum vax_int1 [aw=wgt]
local m1 = `r(mean)'*100
qui sum vax_int2 [aw=wgt]
local m2 = `r(mean)'*100
qui sum vax_int3 [aw=wgt]
local m3 = `r(mean)'*100
qui sum vax_int4 [aw=wgt]
local m4 = `r(mean)'*100
qui asdoc, row(Avg., -, `m1', `m2', `m3', `m4') dec(1) save($output/Sample_desc_DV.doc)

count if vax2!=.
local n0 = r(N)
count if vax_int1==1
local n1 = r(N)
count if vax_int2==1
local n2 = r(N)
count if vax_int3==1
local n3 = r(N)
count if vax_int4==1
local n4 = r(N)

qui asdoc, row(N, `n0', `n1', `n2', `n3', `n4') dec(0) save($output/Sample_desc_DV.doc)
asdoc, text(Data: Preliminary SHARE wave 9 COVID-19 Survey 2 release 0 data.) save($output/Sample_desc_DV.doc)


* b2) Independent variables
sum agecat? female? educ? ///
	health3? disease2? mental2? affected? ///
	urban? makeend_new? inc2? empl_new?

* Create heading for table: sozdem
asdoc, row(Country, Number of respondents, 50-64 years, 65-79 years, 80+ years, Male, Female, Primary, Secondary, Post-secondary) ///
	replace title(Sample distribution: Socio-demographic determinants) ///
	save($output/Sample_desc_IVs_sozdem.doc)

* Add each characteristic row by row by looping
levelsof cnt, local(cnt)
foreach c of local cnt {
	
	noi di "`c'"
	
	* Number of respondents
	qui sum vax2 if cnt=="`c'" [aw=wgt]
	local m = `r(N)'
	asdoc, accum(`m')
	
	* IVS
	foreach var of varlist agecat? female? educ? {
		
		qui sum `var' if cnt=="`c'" [aw=wgt]
		local m = `r(mean)'*100
		asdoc, accum(`m')
	}
	qui asdoc, row(`c', $accum) dec(1) save($output/Sample_desc_IVs_sozdem.doc)
}

* Add numbers
count if vax2!=.
local n0 = r(N)

local n = 1
foreach var of varlist agecat? female? educ? {
	qui sum `var' [aw=wgt]
	local m`n' = `r(mean)'*100
	count if `var'==1
	local n`n' = r(N)
	local n = `n'+1
}
qui asdoc, row(Avg., -, `m1', `m2', `m3', `m4', `m5', `m6', `m7', `m8') dec(1) save($output/Sample_desc_IVs_sozdem.doc)
qui asdoc, row(N, `n0', `n1', `n2', `n3', `n4', `n5', `n6', `n7', `n8') dec(0) save($output/Sample_desc_IVs_sozdem.doc)
asdoc, text(Data: Preliminary SHARE wave 9 COVID-19 Survey 2 release 0 data.) save($output/Sample_desc_IVs_sozdem.doc)


* Create heading for table: health
asdoc, row(Country, Number of respondents, Poor, Fair/good, Very good/excellent, No, Yes, No, Yes, Not affected, Mildly affected, Severely affected) ///
	replace title(Sample distribution: Health-related determinants) ///
	save($output/Sample_desc_IVs_health.doc)

* Add each characteristic row by row by looping
levelsof cnt, local(cnt)
foreach c of local cnt {
	
	noi di "`c'"
	
	* Number of respondents
	qui sum vax2 if cnt=="`c'" [aw=wgt]
	local m = `r(N)'
	asdoc, accum(`m')
	
	* IVS
	foreach var of varlist health3? disease2? mental2? affected? {
		
		qui sum `var' if cnt=="`c'" [aw=wgt]
		local m = `r(mean)'*100
		asdoc, accum(`m')
	}
	qui asdoc, row(`c', $accum) dec(1) save($output/Sample_desc_IVs_health.doc)
}

* Add numbers
count if vax2!=.
local n0 = r(N)

local n = 1
foreach var of varlist health3? disease2? mental2? affected? {
	qui sum `var' [aw=wgt]
	local m`n' = `r(mean)'*100
	count if `var'==1
	local n`n' = r(N)
	local n = `n'+1
}
qui asdoc, row(Avg., -, `m1', `m2', `m3', `m4', `m5', `m6', `m7', `m8', `m9', `m10') dec(1) save($output/Sample_desc_IVs_health.doc)
qui asdoc, row(N, `n0', `n1', `n2', `n3', `n4', `n5', `n6', `n7', `n8', `n9', `n10') dec(0) save($output/Sample_desc_IVs_health.doc)
asdoc, text(Data: Preliminary SHARE wave 9 COVID-19 Survey 2 release 0 data.) save($output/Sample_desc_IVs_health.doc)


* Create heading for table: living
asdoc, row(Country, Number of respondents, Rural, Urban, With difficulties, Without difficulties, No, Yes, Retired, Employed/self-employed, Unemployed, Other non-working status) ///
	replace title(Sample distribution: Determinants related to living conditions and economic situation) ///
	save($output/Sample_desc_IVs_living.doc)

* Add each characteristic row by row by looping
levelsof cnt, local(cnt)
foreach c of local cnt {
	
	noi di "`c'"
	
	* Number of respondents
	qui sum vax2 if cnt=="`c'" [aw=wgt]
	local m = `r(N)'
	asdoc, accum(`m')
	
	* IVS
	foreach var of varlist urban? makeend_new? inc21 inc22 empl_new? {
		
		qui sum `var' if cnt=="`c'" [aw=wgt]
		local m = `r(mean)'*100
		asdoc, accum(`m')
	}
	qui asdoc, row(`c', $accum) dec(1) save($output/Sample_desc_IVs_living.doc)
}

* Add numbers
count if vax2!=.
local n0 = r(N)

local n = 1
foreach var of varlist urban? makeend_new? inc2? empl_new? {
	qui sum `var' [aw=wgt]
	local m`n' = `r(mean)'*100
	count if `var'==1
	local n`n' = r(N)
	local n = `n'+1
}
qui asdoc, row(Avg., -, `m1', `m2', `m3', `m4', `m5', `m6', `m7', `m8', `m9', `m10') dec(1) save($output/Sample_desc_IVs_living.doc)
qui asdoc, row(N, `n0', `n1', `n2', `n3', `n4', `n5', `n6', `n7', `n8', `n9', `n10') dec(0) save($output/Sample_desc_IVs_living.doc)
asdoc, text(Data: Preliminary SHARE wave 9 COVID-19 Survey 2 release 0 data.) save($output/Sample_desc_IVs_living.doc)



*******************************************************************************
** c) Overview of not yet vaccinated respondents: willing, undecided, unwilling
*******************************************************************************
table country if vax==0 [aw=wgt], ///
	c(n vax mean vax_int2 mean vax_int3 mean vax_int4) row format(%8.3f)



*************************************************************
** d) Overview of not yet vaccinated respondents by age group
*************************************************************
table country if agecat==1 [aw=wgt], ///
	c(n vax mean vax_int1) row format(%8.3f)
table country if agecat==2 [aw=wgt], ///
	c(n vax mean vax_int1) row format(%8.3f)
table country if agecat==3 [aw=wgt], ///
	c(n vax mean vax_int1) row format(%8.3f)



************************************************
** e) Overwiew across countries with trimmed CIs
************************************************
tab vax, m
logit vax i.country [pw=wgt]
margins, over(country) post
est sto e
sum vax [aw=wgt]	// 82.3%
coefplot e, recast(bar) hor barw(.7) citop ciopt(recast(rcap) lcol(gs0) lw(thin)) ///
	title("", size(4)) ///
	ylab(, labs(3) angle(0) nogrid) grid(none) ///
	xtitle("in %") xlab(0 "0" .2 "20" .4 "40" .6 "60" .8 "80" 1 "100", labs(3)) ///
	xline(`r(mean)', lpat(dash)) ///
	p1(fcol(gs6) lcol(gs0) lwidth(.1)) ///
	legend(off region(lcol(white))) sort(.:.) ///
	note("Data: Preliminary SHARE wave 9 COVID-19 Survey 2 release 0 data (n=46,989; weighted) with 95%-confidence intervals.", size(2.2) span)
graph save $output\vax_country.gph, replace
graph export $output\vax_country.emf, replace



********************
** f) Stacked graph
********************
table country [aw=wgt], c(mean vax_int1 mean vax_int2 mean vax_int3 mean vax_int4) ///
	format(%8.2f) row
tab1 vax_int?

graph hbar vax_int? [aw=wgt], over(country, ///
	sort(1) des rev gap(60) lab(labs(3))) stack ///
	title("", size(3)) ///
	ytitle("in %", size(3)) ///
	ylab(0 "0" .2 "20" .4 "40" .6 "60" .8 "80" 1 "100", labs(3) nogrid) ///
	bar(1, fcol(green) lcol(gs0) lwidth(.01)) ///
	bar(2, fcol(yellow) lcol(gs0) lwidth(.01)) ///
	bar(3, fcol(orange) lcol(gs0) lwidth(.01)) ///
	bar(4, fcol(red) lcol(gs0) lwidth(.01)) ///
	legend(order(1 "I am already vaccinated" 2 "I want to be vaccinated" ///
		3 "I'm still undecided" 4 "I don't want to be vaccinated") ///
		row(2) size(2.5) region(lcol(white)) margin(vsmall)) ///
	plotregion(margin(vsmall)) ///
	note("Data: Preliminary SHARE wave 9 COVID-19 Survey 2 release 0 data (n=46,968; weighted).", size(2.3) span)
graph save $output\vax_stacked.gph, replace
graph export $output\vax_stacked.emf, replace



*******************************
* g) Excel output for coefficients
*******************************
tab1 vax_int?, m

cap drop id
bysort country (mergeid): gen id = _n

* vaccinated (Fig. 1)
logit vax_int1 i.country [pw=wgt], vce(robust)
margins, over(country) post
matrix m = r(table)
matlist m
local s = 1
local n = 0
gen b_vax_int1 = .
gen se_vax_int1 = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_vax_int1 = m[1,`s'+`n'] if country==`c'
	replace se_vax_int1 = m[2,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_vax_int1 se_vax_int1 if id==1 ///
	using "${output}/coeff_vax_int.xlsx", ///
	sheet(vaccinated, modify) cell(A4)

* willing
logit vax_int2 i.country [pw=wgt], vce(robust)
margins, over(country) post
matrix m = r(table)
matlist m
local s = 1
local n = 0
gen b_vax_int2 = .
gen se_vax_int2 = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_vax_int2 = m[1,`s'+`n'] if country==`c'
	replace se_vax_int2 = m[2,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_vax_int2 se_vax_int2 if id==1 ///
	using "${output}/coeff_vax_int.xlsx", ///
	sheet(willing, modify) cell(A4)

* undecided
logit vax_int3 i.country [pw=wgt], vce(robust)
margins, over(country) post
matrix m = r(table)
matlist m
local s = 1
local n = 0
gen b_vax_int3 = .
gen se_vax_int3 = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_vax_int3 = m[1,`s'+`n'] if country==`c'
	replace se_vax_int3 = m[2,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_vax_int3 se_vax_int3 if id==1 ///
	using "${output}/coeff_vax_int.xlsx", ///
	sheet(undecided, modify) cell(A4)	

* unwilling
logit vax_int4 i.country [pw=wgt], vce(robust)
margins, over(country) post
matrix m = r(table)
matlist m
local s = 1
local n = 0
gen b_vax_int4 = .
gen se_vax_int4 = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_vax_int4 = m[1,`s'+`n'] if country==`c'
	replace se_vax_int4 = m[2,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_vax_int4 se_vax_int4 if id==1 ///
	using "${output}/coeff_vax_int.xlsx", ///
	sheet(unwilling, modify) cell(A4)

cap drop b_vax_int? se_vax_int?





********************************************************************************
*** 2) Bivariate analyses of determinants
********************************************************************************

** Compile marginal effects
sum vax_int_ny [aw=wgt]	// 13.7%

eststo clear
local e=1
foreach iv of varlist agecat female educ ///
	health3 disease2 mental2 affected ///
	urban makeend_new inc3 empl ///
	/*flu internet*/ {
	logit vax_int_ny i.`iv' [pw=wgt], vce(robust)
	margins, over(`iv') post 
	*margins, dydx(`iv')
	est sto e`e'
	local e = `e'+1
}


************************
** a) Socio-demographics
************************
sum agecat female educ internet if vax_int_ny!=. [aw=wgt]
sum vax_int_ny [aw=wgt]	// 13.7%
coefplot e1 e2 e3 /*e12*/, ///
	recast(bar) hor barw(.7) citop ciopt(recast(rcap) lcol(gs0) lw(thin)) ///
	title("", size(4)) ///
	ylab(, labs(3) angle(0) nogrid) grid(none) ///
	xtitle("in %") xlab(0 "0" .05 "5" .1 "10" .15 "15" .2 "20", labs(3)) ///
	xline(`r(mean)', lpat(dash)) ///
	coeflabels(1.agecat="50-64" 2.agecat="65-79" 3.agecat="80+" ///
		0.female="Male" 1.female="Female" ///
		1.educ="Primary" 2.educ="Secondary" 3.educ="Post-secondary" ///
		/*0.internet="No" 1.internet="Yes"*/, ///
		notick labsize(2.5) labcol(gs0) labgap(.5)) ///
	headings(1.agecat="{bf:Age categories}" ///
		0.female="{bf:Gender}" ///
		1.educ="{bf:Level of education}" ///
		/*0.internet="{bf:Internet usage}"*/, gap(1)) ///
	p1(fcol(gs6) lcol(gs0) lwidth(.1)) ///
	p2(fcol(gs6) lcol(gs0) lwidth(.1)) ///
	p3(fcol(gs6) lcol(gs0) lwidth(.1)) ///
	/*p4(fcol(gs6) lcol(gs0) lwidth(.1))*/ ///
	legend(off region(lcol(white))) ///
	plotregion(margin(vsmall)) ///
	note("Data: Preliminary SHARE wave 9 COVID-19 Survey 2 release 0 data (n=46,111-46.968; weighted) with 95%-confidence intervals.", size(2.2) span)
graph save $output\vax_sociodem.gph, replace
graph export $output\vax_sociodem.emf, replace

* Checks
table country female [aw=wgt], c(mean vax_int_ny) row
logit vax_int_ny i.female##i.country [pw=wgt]
margin country, dydx(female)
logit vax_int_ny female [pw=wgt]
logit vax_int_undec female [pw=wgt]
logit vax_int_no female [pw=wgt]
// females are not more undecided, but refusing at higher numbers!



***************************
** b) Health & Affectedness
***************************
sum health3 disease2 mental2 affected /*flu*/ if vax_int_ny!=. [aw=wgt]
sum vax_int_ny [aw=wgt]	// 13.7%
coefplot e4 e5 e6 e7, ///
	recast(bar) hor barw(.7) citop ciopt(recast(rcap) lcol(gs0) lw(thin)) ///
	title("", size(4)) ///
	ylab(, labs(3) angle(0) nogrid) grid(none) ///
	xtitle("in %") xlab(0 "0" .05 "5" .1 "10" .15 "15" .2 "20", labs(3)) ///
	xline(`r(mean)', lpat(dash)) ///
	coeflabels(0.health3="Poor/fair" 1.health3="Good" 2.health3="Very good/excellent" ///
		0.disease2="No" 1.disease2="Yes" ///
		0.mental2="No" 1.mental2="Yes" ///
		0.affected="No" ///
		1.affected="Mildly" ///
		2.affected="Severely", ///
		notick labsize(2.5) labcol(gs0) labgap(.5)) ///
	headings(0.health3="{bf:Self-rated health}" ///
		0.disease2="{bf:Diagnosed physical illnesses}" ///
		0.mental2="{bf:Mental health issues}" ///
		0.affected="{bf:Affectedness by COVID-19}", gap(1)) ///
	p1(fcol(gs6) lcol(gs0) lwidth(.1)) ///
	p2(fcol(gs6) lcol(gs0) lwidth(.1)) ///
	p3(fcol(gs6) lcol(gs0) lwidth(.1)) ///
	p4(fcol(gs6) lcol(gs0) lwidth(.1)) ///
	legend(off region(lcol(white))) ///
	plotregion(margin(vsmall)) ///
	note("Data: Preliminary SHARE wave 9 COVID-19 Survey 2 release 0 data (n=46,880-46,962; weighted) with 95%-confidence intervals.", size(2.2) span)
graph save $output\vax_health.gph, replace
graph export $output\vax_health.emf, replace

* Checks
logit vax_int_ny depres anx sleep lonely2 [pw=wgt]
// only loneliness is (positively) correlated with not being vaccinated
tab1 cah004_?
logit vax_int_ny cah004_? [pw=wgt]
// hip fracture does not fit!
logit vax_int_ny i.health [pw=wgt]
logit vax_int_undec i.health [pw=wgt]
logit vax_int_no i.health [pw=wgt]

logit vax_int_undec i.health3 [pw=wgt]
logit vax_int_ny i.health3 if agecat==3 [pw=wgt]
// 80+ mit guter Gesundheit lassen sich eher impfen?!



**********************
** c) Living situation
**********************
sum urban makeend_new inc2 inc3 empl if vax_int_ny!=. [aw=wgt]
sum vax_int_ny [aw=wgt]
coefplot e8 e9 e10 e11, ///
	recast(bar) hor barw(.7) citop ciopt(recast(rcap) lcol(gs0) lw(thin)) ///
	title("", size(4)) drop(9.inc3) ///
	ylab(, labs(3) angle(0) nogrid) grid(none) ///
	xtitle("in %") xlab(0 "0" .05 "5" .1 "10" .15 "15" .2 "20" .25 "25" .3 "30", labs(3)) ///
	xline(`r(mean)', lpat(dash)) ///
	coeflabels(0.urban="Rural" 1.urban="Urban" ///
		0.makeend_new="(Fairly) easily" ///
		1.makeend_new="With great/some difficulties" ///
		0.inc3="No" ///
		1.inc3="Yes" ///
		1.empl="Retired" ///
		2.empl="Employed/self-employed" ///
		3.empl="Other non-working", ///
		notick labsize(2.5) labcol(gs0) labgap(.5)) ///
	headings(0.urban="{bf:Area of living}" ///
		0.makeend_new="{bf:Making ends meet}" ///
		0.inc3="{bf:At risk of poverty}" ///
		1.empl="{bf:Working status}", gap(1)) ///
	p1(fcol(gs6) lcol(gs0) lwidth(.1)) ///
	p2(fcol(gs6) lcol(gs0) lwidth(.1)) ///
	p3(fcol(gs6) lcol(gs0) lwidth(.1)) ///
	p4(fcol(gs6) lcol(gs0) lwidth(.1)) ///
	legend(off region(lcol(white))) ///
	plotregion(margin(vsmall)) ///
	note("Data: Preliminary SHARE wave 9 COVID-19 Survey 2 release 0 data (n=45,206-46,931; weighted) with 95%-confidence intervals.", size(2.2) span)
graph save $output\vax_living.gph, replace
graph export $output\vax_living.emf, replace



*********************************
** Excel output for coefficients
*********************************

* agecat
logit vax_int_ny i.agecat [pw=wgt], vce(robust)
margins, over(agecat) post	
matrix m = r(table)
matlist m
foreach num of numlist 1/3 {
	local num2 = `num'+3
	gen b = m[1,`num']
	gen se = m[2,`num']
	export excel b se if _n==1 ///
		using "${output}/coeff_bivariate.xlsx", ///
		sheet(agecat, modify) cell(A`num2')
	cap drop b se
}

* female
logit vax_int_ny i.female [pw=wgt], vce(robust)
margins, over(female) post
matrix m = r(table)
matlist m
foreach num of numlist 1/2 {
	local num2 = `num'+3
	gen b = m[1,`num']
	gen se = m[2,`num']
	export excel b se if _n==1 ///
		using "${output}/coeff_bivariate.xlsx", ///
		sheet(female, modify) cell(A`num2')
	cap drop b se
}

* educ
logit vax_int_ny i.educ [pw=wgt], vce(robust)
margins, over(educ) post	
matrix m = r(table)
matlist m
foreach num of numlist 1/3 {
	local num2 = `num'+3
	gen b = m[1,`num']
	gen se = m[2,`num']
	export excel b se if _n==1 ///
		using "${output}/coeff_bivariate.xlsx", ///
		sheet(educ, modify) cell(A`num2')
	cap drop b se
}

* internet
logit vax_int_ny i.internet [pw=wgt], vce(robust)
margins, over(internet) post
matrix m = r(table)
matlist m
foreach num of numlist 1/2 {
	local num2 = `num'+3
	gen b = m[1,`num']
	gen se = m[2,`num']
	export excel b se if _n==1 ///
		using "${output}/coeff_bivariate.xlsx", ///
		sheet(internet, modify) cell(A`num2')
	cap drop b se
}

* health
logit vax_int_ny i.health3 [pw=wgt], vce(robust)
margins, over(health3) post	
matrix m = r(table)
matlist m
foreach num of numlist 1/3 {
	local num2 = `num'+3
	gen b = m[1,`num']
	gen se = m[2,`num']
	export excel b se if _n==1 ///
		using "${output}/coeff_bivariate.xlsx", ///
		sheet(health, modify) cell(A`num2')
	cap drop b se
}

* disease
logit vax_int_ny i.disease2 [pw=wgt], vce(robust)
margins, over(disease2) post
matrix m = r(table)
matlist m
foreach num of numlist 1/2 {
	local num2 = `num'+3
	gen b = m[1,`num']
	gen se = m[2,`num']
	export excel b se if _n==1 ///
		using "${output}/coeff_bivariate.xlsx", ///
		sheet(disease, modify) cell(A`num2')
	cap drop b se
}

* mental
logit vax_int_ny i.mental2 [pw=wgt], vce(robust)
margins, over(mental2) post
matrix m = r(table)
matlist m
foreach num of numlist 1/2 {
	local num2 = `num'+3
	gen b = m[1,`num']
	gen se = m[2,`num']
	export excel b se if _n==1 ///
		using "${output}/coeff_bivariate.xlsx", ///
		sheet(mental, modify) cell(A`num2')
	cap drop b se
}

* affected
logit vax_int_ny i.affected [pw=wgt], vce(robust)
margins, over(affected) post	
matrix m = r(table)
matlist m
foreach num of numlist 1/3 {
	local num2 = `num'+3
	gen b = m[1,`num']
	gen se = m[2,`num']
	export excel b se if _n==1 ///
		using "${output}/coeff_bivariate.xlsx", ///
		sheet(affected, modify) cell(A`num2')
	cap drop b se
}

* urban
logit vax_int_ny i.urban [pw=wgt], vce(robust)
margins, over(urban) post
matrix m = r(table)
matlist m
foreach num of numlist 1/2 {
	local num2 = `num'+3
	gen b = m[1,`num']
	gen se = m[2,`num']
	export excel b se if _n==1 ///
		using "${output}/coeff_bivariate.xlsx", ///
		sheet(urban, modify) cell(A`num2')
	cap drop b se
}

* makeend
logit vax_int_ny i.makeend_new [pw=wgt], vce(robust)
margins, over(makeend_new) post	
matrix m = r(table)
matlist m
foreach num of numlist 1/2 {
	local num2 = `num'+3
	gen b = m[1,`num']
	gen se = m[2,`num']
	export excel b se if _n==1 ///
		using "${output}/coeff_bivariate.xlsx", ///
		sheet(makeend, modify) cell(A`num2')
	cap drop b se
}

* inc2
logit vax_int_ny i.inc2 [pw=wgt], vce(robust)
margins, over(inc2) post	
matrix m = r(table)
matlist m
foreach num of numlist 1/2 {
	local num2 = `num'+3
	gen b = m[1,`num']
	gen se = m[2,`num']
	export excel b se if _n==1 ///
		using "${output}/coeff_bivariate.xlsx", ///
		sheet(inc2, modify) cell(A`num2')
	cap drop b se
}

* inc3
logit vax_int_ny i.inc3 [pw=wgt], vce(robust)
margins, over(inc3) post	
matrix m = r(table)
matlist m
foreach num of numlist 1/2 {
	local num2 = `num'+3
	gen b = m[1,`num']
	gen se = m[2,`num']
	export excel b se if _n==1 ///
		using "${output}/coeff_bivariate.xlsx", ///
		sheet(inc3, modify) cell(A`num2')
	cap drop b se
}

* empl
logit vax_int_ny i.empl [pw=wgt], vce(robust)
margins, over(empl) post	
matrix m = r(table)
matlist m
foreach num of numlist 1/4 {
	local num2 = `num'+3
	gen b = m[1,`num']
	gen se = m[2,`num']
	export excel b se if _n==1 ///
		using "${output}/coeff_bivariate.xlsx", ///
		sheet(empl, modify) cell(A`num2')
	cap drop b se
}





********************************************************************************
*** 3) Multivariate analyses of determinants
********************************************************************************

****************************************************
** Logit model: not vaccinated (undecided/unwilling)
****************************************************
logit vax_int_ny agecat2 agecat3 female educ2 educ3 /*internet*/ ///
	health32 health33 disease2 mental2 affected2 affected3 ///
	urban makeend_new inc32 inc33 empl2 empl3 ///
	i.country [pw=wgt], vce(robust)
// n=43,388 (incl. missing income!), R²=.21
center agecat2 agecat3 female educ2 educ3 ///
	health32 health33 disease2 mental2 affected2 affected3 ///
	urban makeend_new inc32 inc33 empl2 empl3 [aw=wgt] if e(sample), st replace
sum c_* [aw=wgt] if e(sample)

eststo clear
logit vax_int_ny c_agecat2 c_agecat3 c_female c_educ2 c_educ3 ///
	c_health32 c_health33 c_disease2 c_mental2 c_affected2 c_affected3 ///
	c_urban c_makeend_new c_inc32 c_inc33 c_empl2 c_empl3 ///
	i.country [pw=wgt], vce(robust)
esttab using $output\Logit_vax_int.rtf, replace ///
	b(%8.3f) t(%8.2f) nonum compress nogaps varwidth(20) modelwidth(10)
coefplot, drop(_cons *country *inc33) xline(0, lwidth(thin) lcol(gs0) lpattern(dash)) ///
	mcol(gs0) msize(small) ciopts(lcol(gs5)) ///
	xtitle("Effect on Pr(not vaccinated)", size(2.5)) ///
	coeflabels(c_agecat2="Age (65-79)" c_agecat3="Age (80+)" ///
		c_female="Female" ///
		c_educ2="Secondary" ///
		c_educ3="Post-secondary" ///
		c_health32="Good" ///
		c_health33="Very good/excellent" ///
		c_disease2="Yes" ///
		c_mental2="Yes" ///
		c_affected2="Mildly" ///
		c_affected3="Severely" ///
		c_urban="Urban area" ///
		c_makeend_new="With great/some difficulties" ///
		c_inc32="Yes" ///
		c_empl2="Employed or self-employed" ///
		c_empl3="Other non-working", ///
		notick labsize(2) labcol(gs0) labgap(.5)) ///
	headings(c_agecat2="{bf:Age categories (ref.: 50-64)}" ///
		c_female="{bf:Gender (ref.: Male)}" ///
		c_educ2="{bf:Level of education (ref.: Primary)}" ///
		c_internet="{bf:Internet usage (ref.: No)}" ///
		c_health32="{bf:Self-rated health (ref.: Poor/fair)}" ///
		c_disease2="{bf:Diagnosed physical illnesses (ref.: No)}" ///
		c_mental2="{bf:Mental health issues (ref.: No)}" ///
		c_affected2="{bf:Affected by COVID-19 (ref.: No)}" ///
		c_urban="{bf:Area of living (ref.: Rural area)}" ///
		c_makeend_new="{bf:Make ends meet (ref.: (Fairly) easily)}" ///
		c_inc32="{bf:At risk of poverty (ref.: No)}" ///
		c_empl2="{bf:Working status (ref.: Retired)}", gap(.2)) ///
	groups(headroom c_agecat3="{bf:Socio-dem.}" ///
		c_disease2="{bf:Health}" ///
		c_inc32= "{bf:Living conditions}", labcol(orange)) ///
	xlab(-.6(.2).6, labs(2.5)) ///
	scheme(s2color) graphregion(color(white)) ///
	note("Data: Preliminary SHARE wave 9 COVID-19 Survey 2 release 0 data (n=43,388, weighted)." "Note: Displayed are standardized logistic regression coefficients from a multivariate model, incl. country predictors and income item non-" "         response (not shown) with 95%-confidence intervals.", size(2.0) span)
graph save $output\logit_vax_int.gph, replace
graph export $output\logit_vax_int.emf, replace


**********************************
* Logit model: I'm still undecided
**********************************
logit vax_int3 agecat2 agecat3 female educ2 educ3 ///
	health32 health33 disease2 mental2 affected2 affected3 ///
	urban makeend_new inc32 inc33 empl2 empl3 ///
	i.country [pw=wgt], vce(robust)
// n=42,848

eststo clear
logit vax_int3 agecat2 agecat3 female educ2 educ3 ///
	health32 health33 disease2 mental2 affected2 affected3 ///
	urban makeend_new inc2 empl2 empl3 ///
	i.country [pw=wgt], vce(robust)
esttab using $output\Logit_vax_undec.rtf, replace ///
	b(%8.3f) t(%8.2f) nonum compress nogaps varwidth(20) modelwidth(10)
coefplot, drop(_cons *country) xline(0, lwidth(thin) lcol(gs0) lpattern(dash)) ///
	mcol(gs0) msize(small) ciopts(lcol(gs5)) ///
	xtitle("Effect on Pr(not vaccinated)", size(2.5)) ///
	coeflabels(agecat2="Age (65-79)" agecat3="Age (80+)" ///
		female="Female" ///
		educ2="Secondary" ///
		educ3="Post-secondary" ///
		health32="Fair/good" ///
		health33="Very good/excellent" ///
		disease2="Yes" ///
		mental2="Yes" ///
		affected2="Mildly" ///
		affected3="Severely" ///
		urban="Urban area" ///
		makeend_new="With great/some difficulties" ///
		inc2="Yes" ///
		empl2="Employed or self-employed" ///
		empl3="Other non-working", ///
		notick labsize(2) labcol(gs0) labgap(.5)) ///
	headings(agecat2="{bf:Age categories (ref.: 50-64)}" ///
		female="{bf:Gender (ref.: Male)}" ///
		educ2="{bf:Level of education (ref.: Primary)}" ///
		health32="{bf:Self-rated health (ref.: Poor)}" ///
		disease2="{bf:Diagnosed physical illnesses (ref.: No)}" ///
		mental2="{bf:Mental health issues (ref.: No)}" ///
		affected2="{bf:Affected by COVID-19 (ref.: No)}" ///
		urban="{bf:Area of living (ref.: Rural area)}" ///
		makeend_new="{bf:Make ends meet (ref.: (Fairly) easily)}" ///
		inc2="{bf:At risk of poverty (ref.: No)}" ///
		empl2="{bf:Working status (ref.: Retired)}", gap(.2)) ///
	groups(headroom agecat3="{bf:Socio-dem.}" ///
		disease2="{bf:Health}" ///
		inc2= "{bf:Living conditions}", labcol(orange)) ///
	xlab(-1(.2)1, labs(2.5)) ///
	scheme(s2color) graphregion(color(white)) ///
	note("Data: Preliminary SHARE wave 9 COVID-19 Survey 2 release 0 data (n=42,848, weighted)." "Note: Displayed are logistic regression coefficients from a multivariate model, incl. country predictors (not shown) with 95%-confidence intervals.", size(2.0) span)
graph save $output\logit_vax_undec.gph, replace
graph export $output\logit_vax_undec.emf, replace


********************************************
* Logit model: I don't want to be vaccinated
********************************************
logit vax_int4 agecat2 agecat3 female educ2 educ3 ///
	health32 health33 disease2 mental2 affected2 affected3 ///
	urban makeend_new inc32 inc33 empl2 empl3 ///
	i.country [pw=wgt], vce(robust)
// n=43,388

eststo clear
logit vax_int4 agecat2 agecat3 female educ2 educ3 ///
	health32 health33 disease2 mental2 affected2 affected3 ///
	urban makeend_new inc2 empl2 empl3 ///
	i.country [pw=wgt], vce(robust)
esttab using $output\Logit_vax_no.rtf, replace ///
	b(%8.3f) t(%8.2f) nonum compress nogaps varwidth(20) modelwidth(10)
coefplot, drop(_cons *country) xline(0, lwidth(thin) lcol(gs0) lpattern(dash)) ///
	mcol(gs0) msize(small) ciopts(lcol(gs5)) ///
	xtitle("Effect on Pr(not vaccinated)", size(2.5)) ///
	coeflabels(agecat2="Age (65-79)" agecat3="Age (80+)" ///
		female="Female" ///
		educ2="Secondary" ///
		educ3="Post-secondary" ///
		health32="Fair/good" ///
		health33="Very good/excellent" ///
		disease2="Yes" ///
		mental2="Yes" ///
		affected2="Mildly" ///
		affected3="Severely" ///
		urban="Urban area" ///
		makeend_new="With great/some difficulties" ///
		inc2="Yes" ///
		empl2="Employed or self-employed" ///
		empl3="Other non-working status", ///
		notick labsize(2) labcol(gs0) labgap(.5)) ///
	headings(agecat2="{bf:Age categories (ref.: 50-64)}" ///
		female="{bf:Gender (ref.: Male)}" ///
		educ2="{bf:Level of education (ref.: Primary)}" ///
		health32="{bf:Self-rated health (ref.: Poor)}" ///
		disease2="{bf:Diagnosed physical illnesses (ref.: No)}" ///
		mental2="{bf:Mental health issues (ref.: No)}" ///
		affected2="{bf:Affected by COVID-19 (ref.: No)}" ///
		urban="{bf:Area of living (ref.: Rural area)}" ///
		makeend_new="{bf:Make ends meet (ref.: (Fairly) easily)}" ///
		inc2="{bf:At risk of poverty (ref.: No)}" ///
		empl2="{bf:Working status (ref.: Retired)}", gap(.2)) ///
	groups(headroom agecat3="{bf:Socio-dem.}" ///
		disease2="{bf:Health}" ///
		inc2= "{bf:Living conditions}", labcol(orange)) ///
	xlab(-1(.2)1, labs(2.5)) ///
	scheme(s2color) graphregion(color(white)) ///
	note("Data: Preliminary SHARE wave 9 COVID-19 Survey 2 release 0 data (n=43,388, weighted)." "Note: Displayed are logistic regression coefficients from a multivariate model, incl. country predictors (not shown) with 95%-confidence intervals.", size(2.0) span)
graph save $output\logit_vax_no_int.gph, replace
graph export $output\logit_vax_no_int.emf, replace





********************************************************************************
*** 4) Country differences
********************************************************************************

sum agecat female educ ///
	health3 disease2 mental2 affected ///
	urban makeend_new inc2 empl_new

cap drop id
bysort country (mergeid): gen id = _n
	
* agecat
eststo clear
reg vax_int_ny i.agecat##i.country [pw=wgt]
margins r.agecat, over(country)
matrix m = r(table)
matlist m
local s = 1
local n = 0
gen b_agecat2 = .
gen se_agecat2 = .
gen z_agecat2 = .
gen p_agecat2 = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_agecat2 = m[1,`s'+`n'] if country==`c'
	replace se_agecat2 = m[2,`s'+`n'] if country==`c'
	replace z_agecat2 = m[3,`s'+`n'] if country==`c'
	replace p_agecat2 = m[4,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_agecat2 se_agecat2 z_agecat2 p_agecat2 if id==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(agecat2, modify) cell(A4)

matlist m
local s = 29
local n = 0
gen b_agecat3 = .
gen se_agecat3 = .
gen z_agecat3 = .
gen p_agecat3 = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_agecat3 = m[1,`s'+`n'] if country==`c'
	replace se_agecat3 = m[2,`s'+`n'] if country==`c'
	replace z_agecat3 = m[3,`s'+`n'] if country==`c'
	replace p_agecat3 = m[4,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_agecat3 se_agecat3 z_agecat3 p_agecat3 if id==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(agecat3, modify) cell(A4)

reg vax_int_ny i.agecat [pw=wgt]
margins r.agecat
matrix m = r(table)
matlist m
replace b_agecat2 =  m[1,1]
replace se_agecat2 = m[2,1]
replace z_agecat2 =  m[3,1]
replace p_agecat2 =  m[4,1]

export excel b_agecat2 se_agecat2 z_agecat2 p_agecat2 if _n==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(agecat2, modify) cell(B32)

matlist m
replace b_agecat3 =  m[1,2]
replace se_agecat3 = m[2,2]
replace z_agecat3 =  m[3,2]
replace p_agecat3 =  m[4,2]

export excel b_agecat3 se_agecat3 z_agecat3 p_agecat3 if _n==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(agecat3, modify) cell(B32)
	
/*
coefplot e, recast(bar) hor barw(.5) citop ciopt(recast(rcap) lcol(gs0) lw(thin)) ///
	title("", size(4)) ///
	ylab(, labs(3) angle(0) nogrid) grid(none) ///
	xtitle("Marginal effect on Pr(not vaccinated) in %-points", size(3)) ///
	xlab(-.1 "10" 0 "0" .1 "10" .2 "20" .3 "30", labs(3)) ///
	p1(fcol(gs6) lcol(gs0) lwidth(.1)) ///
	legend(off region(lcol(white))) sort(.:.) ///
	note("Data: Preliminary SHARE wave 9 COVID-19 Survey 2 release 0 data(n=46,925, weighted)." "Note: Displayed are average marginal effects with 95%-confidence intervals.", size(2.3) span)
graph save $output\Country_age.gph, replace
graph export $output\Country_age.emf, replace
*/


* female
reg vax_int_ny i.female##i.country [pw=wgt]
margins r.female, over(country)
matrix m = r(table)
matlist m
local s = 1
local n = 0
gen b_female = .
gen se_female = .
gen z_female = .
gen p_female = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_female = m[1,`s'+`n'] if country==`c'
	replace se_female = m[2,`s'+`n'] if country==`c'
	replace z_female = m[3,`s'+`n'] if country==`c'
	replace p_female = m[4,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_female se_female z_female p_female if id==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(female, modify) cell(A4)

reg vax_int_ny i.female [pw=wgt]
margins r.female
matrix m = r(table)
matlist m
replace b_female =  m[1,1]
replace se_female = m[2,1]
replace z_female =  m[3,1]
replace p_female =  m[4,1]

export excel b_female se_female z_female p_female if _n==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(female, modify) cell(B32)	


* educ
reg vax_int_ny i.educ##i.country [pw=wgt]
margins r.educ, over(country)
matrix m = r(table)
matlist m
local s = 1
local n = 0
gen b_educ2 = .
gen se_educ2 = .
gen z_educ2 = .
gen p_educ2 = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_educ2 = m[1,`s'+`n'] if country==`c'
	replace se_educ2 = m[2,`s'+`n'] if country==`c'
	replace z_educ2 = m[3,`s'+`n'] if country==`c'
	replace p_educ2 = m[4,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_educ2 se_educ2 z_educ2 p_educ2 if id==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(educ2, modify) cell(A4)
	
matlist m
local s = 29
local n = 0
gen b_educ3 = .
gen se_educ3 = .
gen z_educ3 = .
gen p_educ3 = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_educ3 = m[1,`s'+`n'] if country==`c'
	replace se_educ3 = m[2,`s'+`n'] if country==`c'
	replace z_educ3 = m[3,`s'+`n'] if country==`c'
	replace p_educ3 = m[4,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_educ3 se_educ3 z_educ3 p_educ3 if id==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(educ3, modify) cell(A4)

reg vax_int_ny i.educ [pw=wgt]
margins r.educ
matrix m = r(table)
matlist m
replace b_educ2 =  m[1,1]
replace se_educ2 = m[2,1]
replace z_educ2 =  m[3,1]
replace p_educ2 =  m[4,1]

export excel b_educ2 se_educ2 z_educ2 p_educ2 if _n==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(educ2, modify) cell(B32)

matlist m
replace b_educ3 =  m[1,2]
replace se_educ3 = m[2,2]
replace z_educ3 =  m[3,2]
replace p_educ3 =  m[4,2]

export excel b_educ3 se_educ3 z_educ3 p_educ3 if _n==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(educ3, modify) cell(B32)


* internet
reg vax_int_ny i.internet##i.country [pw=wgt]
margins r.internet, over(country)
matrix m = r(table)
matlist m
local s = 1
local n = 0
gen b_internet = .
gen se_internet = .
gen z_internet = .
gen p_internet = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_internet = m[1,`s'+`n'] if country==`c'
	replace se_internet = m[2,`s'+`n'] if country==`c'
	replace z_internet = m[3,`s'+`n'] if country==`c'
	replace p_internet = m[4,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_internet se_internet z_internet p_internet if id==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(internet, modify) cell(A4)

reg vax_int_ny i.internet [pw=wgt]
margins r.internet
matrix m = r(table)
matlist m
replace b_internet =  m[1,1]
replace se_internet = m[2,1]
replace z_internet =  m[3,1]
replace p_internet =  m[4,1]

export excel b_internet se_internet z_internet p_internet if _n==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(internet, modify) cell(B32)	


* health
logit vax_int_ny i.health3##i.country [pw=wgt]
margins r.health3, over(country)

matrix m = r(table)
matlist m
local s = 1
local n = 0
gen b_health2 = .
gen se_health2 = .
gen z_health2 = .
gen p_health2 = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_health2 = m[1,`s'+`n'] if country==`c'
	replace se_health2 = m[2,`s'+`n'] if country==`c'
	replace z_health2 = m[3,`s'+`n'] if country==`c'
	replace p_health2 = m[4,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_health2 se_health2 z_health2 p_health2 if id==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(health2, modify) cell(A4)
	
matlist m
local s = 29
local n = 0
gen b_health3 = .
gen se_health3 = .
gen z_health3 = .
gen p_health3 = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_health3 = m[1,`s'+`n'] if country==`c'
	replace se_health3 = m[2,`s'+`n'] if country==`c'
	replace z_health3 = m[3,`s'+`n'] if country==`c'
	replace p_health3 = m[4,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_health3 se_health3 z_health3 p_health3 if id==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(health3, modify) cell(A4)

reg vax_int_ny i.health3 [pw=wgt]
margins r.health3
matrix m = r(table)
matlist m
replace b_health2 =  m[1,1]
replace se_health2 = m[2,1]
replace z_health2 =  m[3,1]
replace p_health2 =  m[4,1]

export excel b_health2 se_health2 z_health2 p_health2 if _n==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(health2, modify) cell(B32)

matlist m
replace b_health3 =  m[1,2]
replace se_health3 = m[2,2]
replace z_health3 =  m[3,2]
replace p_health3 =  m[4,2]

export excel b_health3 se_health3 z_health3 p_health3 if _n==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(health3, modify) cell(B32)	


* disease
reg vax_int_ny i.disease2##i.country [pw=wgt]
margins r.disease2, over(country)

matrix m = r(table)
matlist m
local s = 1
local n = 0
gen b_disease = .
gen se_disease = .
gen z_disease = .
gen p_disease = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_disease = m[1,`s'+`n'] if country==`c'
	replace se_disease = m[2,`s'+`n'] if country==`c'
	replace z_disease = m[3,`s'+`n'] if country==`c'
	replace p_disease = m[4,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_disease se_disease z_disease p_disease if id==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(disease, modify) cell(A4)
	
reg vax_int_ny i.disease2 [pw=wgt]
margins r.disease2
matrix m = r(table)
matlist m
replace b_disease =  m[1,1]
replace se_disease = m[2,1]
replace z_disease =  m[3,1]
replace p_disease =  m[4,1]

export excel b_disease se_disease z_disease p_disease if _n==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(disease, modify) cell(B32)	


* mental
reg vax_int_ny i.mental2##i.country [pw=wgt]
margins r.mental2, over(country)

matrix m = r(table)
matlist m
local s = 1
local n = 0
gen b_mental = .
gen se_mental = .
gen z_mental = .
gen p_mental = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_mental = m[1,`s'+`n'] if country==`c'
	replace se_mental = m[2,`s'+`n'] if country==`c'
	replace z_mental = m[3,`s'+`n'] if country==`c'
	replace p_mental = m[4,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_mental se_mental z_mental p_mental if id==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(mental, modify) cell(A4)
	
reg vax_int_ny i.mental2 [pw=wgt]
margins r.mental2
matrix m = r(table)
matlist m
replace b_mental =  m[1,1]
replace se_mental = m[2,1]
replace z_mental =  m[3,1]
replace p_mental =  m[4,1]

export excel b_mental se_mental z_mental p_mental if _n==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(mental, modify) cell(B32)	


* affected
logit vax_int_ny i.affected##i.country [pw=wgt]
margins r.affected, over(country)

matrix m = r(table)
matlist m
local s = 1
local n = 0
gen b_affected2 = .
gen se_affected2 = .
gen z_affected2 = .
gen p_affected2 = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_affected2 = m[1,`s'+`n'] if country==`c'
	replace se_affected2 = m[2,`s'+`n'] if country==`c'
	replace z_affected2 = m[3,`s'+`n'] if country==`c'
	replace p_affected2 = m[4,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_affected2 se_affected2 z_affected2 p_affected2 if id==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(affected2, modify) cell(A4)

matlist m
local s = 29
local n = 0
gen b_affected3 = .
gen se_affected3 = .
gen z_affected3 = .
gen p_affected3 = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_affected3 = m[1,`s'+`n'] if country==`c'
	replace se_affected3 = m[2,`s'+`n'] if country==`c'
	replace z_affected3 = m[3,`s'+`n'] if country==`c'
	replace p_affected3 = m[4,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_affected3 se_affected3 z_affected3 p_affected3 if id==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(affected3, modify) cell(A4)

reg vax_int_ny i.affected [pw=wgt]
margins r.affected
matrix m = r(table)
matlist m
replace b_affected2 =  m[1,1]
replace se_affected2 = m[2,1]
replace z_affected2 =  m[3,1]
replace p_affected2 =  m[4,1]

export excel b_affected2 se_affected2 z_affected2 p_affected2 if _n==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(affected2, modify) cell(B32)

matlist m
replace b_affected3 =  m[1,2]
replace se_affected3 = m[2,2]
replace z_affected3 =  m[3,2]
replace p_affected3 =  m[4,2]

export excel b_affected3 se_affected3 z_affected3 p_affected3 if _n==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(affected3, modify) cell(B32)	

	
* urban
reg vax_int_ny i.urban##i.country [pw=wgt]
margins r.urban, over(country)
matrix m = r(table)
matlist m
local s = 1
local n = 0
gen b_urban = .
gen se_urban = .
gen z_urban = .
gen p_urban = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_urban = m[1,`s'+`n'] if country==`c'
	replace se_urban = m[2,`s'+`n'] if country==`c'
	replace z_urban = m[3,`s'+`n'] if country==`c'
	replace p_urban = m[4,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_urban se_urban z_urban p_urban if id==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(urban, modify) cell(A4)	
	
reg vax_int_ny i.urban2 [pw=wgt]
margins r.urban2
matrix m = r(table)
matlist m
replace b_urban =  m[1,1]
replace se_urban = m[2,1]
replace z_urban =  m[3,1]
replace p_urban =  m[4,1]

export excel b_urban se_urban z_urban p_urban if _n==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(urban, modify) cell(B32)	


* makeend
reg vax_int_ny i.makeend_new##i.country [pw=wgt]
margins r.makeend_new, over(country)
matrix m = r(table)
matlist m
local s = 1
local n = 0
gen b_makeend = .
gen se_makeend = .
gen z_makeend = .
gen p_makeend = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_makeend = m[1,`s'+`n'] if country==`c'
	replace se_makeend = m[2,`s'+`n'] if country==`c'
	replace z_makeend = m[3,`s'+`n'] if country==`c'
	replace p_makeend = m[4,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_makeend se_makeend z_makeend p_makeend if id==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(makeend, modify) cell(A4)

reg vax_int_ny i.makeend_new [pw=wgt]
margins r.makeend_new
matrix m = r(table)
matlist m
replace b_makeend =  m[1,1]
replace se_makeend = m[2,1]
replace z_makeend =  m[3,1]
replace p_makeend =  m[4,1]

export excel b_makeend se_makeend z_makeend p_makeend if _n==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(makeend, modify) cell(B32)


* inc
logit vax_int_ny i.inc2##i.country [pw=wgt]
margins r.inc2, over(country)
matrix m = r(table)
matlist m
local s = 1
local n = 0
gen b_inc = .
gen se_inc = .
gen z_inc = .
gen p_inc = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_inc = m[1,`s'+`n'] if country==`c'
	replace se_inc = m[2,`s'+`n'] if country==`c'
	replace z_inc = m[3,`s'+`n'] if country==`c'
	replace p_inc = m[4,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_inc se_inc z_inc p_inc if id==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(inc, modify) cell(A4)

reg vax_int_ny i.inc2 [pw=wgt]
margins r.inc2
matrix m = r(table)
matlist m
replace b_inc =  m[1,1]
replace se_inc = m[2,1]
replace z_inc =  m[3,1]
replace p_inc =  m[4,1]

export excel b_inc se_inc z_inc p_inc if _n==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(inc, modify) cell(B32)


* empl
logit vax_int_ny i.empl##i.country [pw=wgt]
margins r.empl, over(country)
matrix m = r(table)
matlist m
local s = 1
local n = 0
gen b_empl2 = .
gen se_empl2 = .
gen z_empl2 = .
gen p_empl2 = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_empl2 = m[1,`s'+`n'] if country==`c'
	replace se_empl2 = m[2,`s'+`n'] if country==`c'
	replace z_empl2 = m[3,`s'+`n'] if country==`c'
	replace p_empl2 = m[4,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_empl2 se_empl2 z_empl2 p_empl2 if id==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(empl2, modify) cell(A4)

matlist m
local s = 29
local n = 0
gen b_empl3 = .
gen se_empl3 = .
gen z_empl3 = .
gen p_empl3 = .
levelsof country, local(cnt)
foreach c of local cnt {
	replace b_empl3 = m[1,`s'+`n'] if country==`c'
	replace se_empl3 = m[2,`s'+`n'] if country==`c'
	replace z_empl3 = m[3,`s'+`n'] if country==`c'
	replace p_empl3 = m[4,`s'+`n'] if country==`c'
	local n = `n'+1
}
export excel country b_empl3 se_empl3 z_empl3 p_empl3 if id==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(empl3, modify) cell(A4)
	
reg vax_int_ny i.empl [pw=wgt]
margins r.empl
matrix m = r(table)
matlist m
replace b_empl2 =  m[1,1]
replace se_empl2 = m[2,1]
replace z_empl2 =  m[3,1]
replace p_empl2 =  m[4,1]

export excel b_empl2 se_empl2 z_empl2 p_empl2 if _n==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(empl2, modify) cell(B32)

matlist m
replace b_empl3 =  m[1,2]
replace se_empl3 = m[2,2]
replace z_empl3 =  m[3,2]
replace p_empl3 =  m[4,2]

export excel b_empl3 se_empl3 z_empl3 p_empl3 if _n==1 ///
	using "${output}/countrydiffs.xlsx", ///
	sheet(empl3, modify) cell(B32)


save "${data}/Vaccinations_Analyses_countrydiffs.dta", replace




