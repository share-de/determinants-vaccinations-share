*Author: Arne Bethmann
*Last updated: Nov 2021
*Source: Release 0 w91 & previous CAPI waves

********************************************************************************
*** Frequency tables for publication using asdoc RTF output
********************************************************************************
clear all
global data "R:\Research_projects\2_SHARE_Covid_Research\_Data\Vaccination\Rel0"
global output "S:\Research_projects\Covid_Research\Vaccination\Determinants\graphs\Rel0"


use "${data}/Vaccinations_Analyses_countries.dta", clear


********************************************************************************
*** 1) Define program for standardized frequency table output
********************************************************************************
program vax_freq_table
	syntax, ///
		variables(string)	 /// List variables that should be put into table
		title(string)		 /// asdoc String for title
		h1(string)			 /// asdoc String for heading 1
		h2(string)			 /// asdoc String for heading 2
		h3(string)			 /// asdoc String for heading 3
		outfile(string)		 /// Output file
		foot(string)         // String for footer

	qui{
	
	noi di ""
	noi di "Generating table: `title'"
	* Add header rows
	local titlerow = "Sample distribution: " + "`title'"
	local hrow1 = "\i, \i, " + "`h1'"
	local hrow2 = "\i, Number of respondents, " + "`h2'"
	local hrow3 = "Country, N, " + "`h3'"

	asdoc, row(`hrow1') replace title(`titlerow') save(`outfile')
	asdoc, row(`hrow2') save(`outfile')
	asdoc, row(`hrow3') save(`outfile')	
	
	* Add each characteristic row by row by looping
	levelsof cnt, local(cnt)
	foreach c of local cnt {
		noi di "`c'"
	
		* Number of respondents
		qui count if cnt=="`c'" & vax_int_ny!=.
		local m = `r(N)'
		asdoc, accum(`m')
	
		* IVS
		foreach var of varlist `variables' {
			qui sum `var' if cnt=="`c'" [aw=wgt]
			*qui sum `var' if cnt=="`c'" & vax_int_ny!=.
			local m = `r(mean)'*100
			asdoc, accum(`m')
			qui count if cnt == "`c'" & `var' == 1 & vax_int_ny!=.
			local m = `r(N)'
			asdoc, accum(`m')
		}
	qui asdoc, row(`c', $accum) dec(1) save(`outfile')
	}
  
	* Add total numbers
	count if vax_int_ny!=.
	local n0 = `r(N)'
	local n = 1
	foreach var of varlist `variables' {
		qui sum `var' [aw=wgt]
		*qui sum `var' if vax_int_ny!=.
		local m = `r(mean)'*100
		asdoc, accum(`m')
		qui count if `var'==1 & vax_int_ny!=.
		local m = `r(N)'
		asdoc, accum(`m')
	}
	qui asdoc, row(Total, `n0', $accum) dec(1) save(`outfile')

	* Add footer row
	asdoc, text(`foot') save(`outfile')

	}
end


********************************************************************************
*** 2) Generate tables
********************************************************************************

** Define output file and footer for all tables freq tables
local foot = "Data: Preliminary SHARE wave 9 COVID-19 Survey 2 release 0 data."

** Freq table DV: Vaccination intention
vax_freq_table, ///
	variables(vax_int?) ///
	title("Vaccination intention") ///
	h1("Vaccination intention, \i, \i, \i, \i, \i, \i, \i") ///
	h2("Already vaccinated, \i, Wants to be vaccinated, \i, Undecided, \i, Does not want to be vaccinated, \i") ///
	h3("%, N, %, N, %, N, %, N") ///
	outfile("$output/freq_dv.doc") ///
	foot(`foot')

	
** Freq table socio-dempgraphics
vax_freq_table, ///
	variables(agecat1 agecat2 agecat3 female1 female2 educ1 educ2 educ3) ///
	title("Socio-demographic determinants") ///
	h1("Age group, \i, \i, \i, \i, \i, Gender, \i, \i, \i, Education, \i, \i, \i, \i, \i") ///
	h2("50-64 years, \i, 65-79 years, \i, 80+ years, \i, Male, \i, Female, \i, Primary, \i, Secondary, \i, Post-secondary, \i") ///
	h3("%, N, %, N, %, N, %, N, %, N, %, N, %, N, %, N") ///
	outfile("$output/freq_demo.doc") ///
	foot(`foot')

	
** Freq table health
vax_freq_table, ///
	variables(health3? disease2? mental2? affected?) ///
	title("Health-related determinants") ///
	h1("Self-rated health, \i, \i, \i, \i, \i, Diagnosed physical illness, \i, \i, \i, Mental health issues, \i, \i, \i, Affectedness by COVID-19, \i, \i, \i, \i, \i") ///
	h2("Poor/fair, \i, Good, \i,  Very good/excellent, \i, No, \i, Yes, \i, No, \i, Yes, \i, No, \i, Mildly, \i, Severely, \i") ///
	h3("%, N, %, N, %, N, %, N, %, N, %, N, %, N, %, N, %, N, %, N") ///
	outfile("$output/freq_health.doc") ///
	foot(`foot')


** Freq table living conditions and economic situation
vax_freq_table, ///
	variables(urban? makeend_new? inc31 inc32 empl?) ///
	title("Determinants related to living conditions and economic situation") ///
	h1("Area of living, \i, \i, \i, Making ends meet, \i, \i, \i, At risk of poverty, \i, \i, \i, Working status, \i, \i, \i, \i, \i") ///
	h2("Rural, \i, Urban, \i,  Fairly easily/easily, \i, With great/some difficulties, \i, No, \i, Yes, \i, Retired, \i, Employed/ self-employed, \i, Other non-working, \i") ///
	h3("%, N, %, N, %, N, %, N, %, N, %, N, %, N, %, N, %, N") ///
	outfile("$output/freq_living.doc") ///
	foot(`foot')

