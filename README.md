# Determinants of SARS-CoV-2 vaccinations in the 50+ population across Europe

Public code repository for the research project "Determinants of SARS-CoV-2 vaccinations in the 50+ population across Europe" by Michael Bergmann, Arne Bethmann, Tessa-Virginia Hannemann and Alexander Schumacher.